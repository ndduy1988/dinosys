//
//  CredentialUtils.swift
//  WMCLoyaltySwift
//
//  Created by Shan Haq on 11/10/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import Foundation

public class CredentialUtils {
    public class func credentials() -> URLCredential {
        let path = Bundle.main.path(forResource: "app", ofType: "p12")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let identity = extractIdentity(certData: data, certPassword: "Dino@1234")
        return getCredentialFromCertSwift(identity: identity)
    }
}


//private helpers
extension CredentialUtils {
    
    internal class func getCredentialFromCertSwift(identity: SecIdentity) -> URLCredential {
        var certificateRef: SecCertificate?
        SecIdentityCopyCertificate(identity, &certificateRef)
        
        let certificate = [certificateRef!]
        let credential = URLCredential(identity: identity, certificates: certificate, persistence: URLCredential.Persistence.forSession)
        return credential
    }
    
    internal class func extractIdentity(certData:Data, certPassword:String) -> SecIdentity {
        
        var identity:SecIdentity!
        var securityError:OSStatus = errSecSuccess
        
        var items: CFArray?
        let certOptions: CFDictionary = [ kSecImportExportPassphrase as String: certPassword ] as CFDictionary;
        
        // import certificate to read its entries
        securityError = SecPKCS12Import(certData as CFData, certOptions, &items);
        
        if securityError == errSecSuccess {
            
            let certItems:CFArray = (items as CFArray!)!;
            let certItemsArray:Array = certItems as Array
            let dict:AnyObject? = certItemsArray.first;
            
            if let certEntry:Dictionary = dict as? Dictionary<String, AnyObject> {
                
                // grab the identity
                let identityPointer:AnyObject? = certEntry["identity"];
                let secIdentityRef:SecIdentity = identityPointer as! SecIdentity!;
                
                // grab the certificate chain
                var certRef:SecCertificate?
                SecIdentityCopyCertificate(secIdentityRef, &certRef);
                let certArray:NSMutableArray = NSMutableArray();
                certArray.add(certRef as SecCertificate!);
                
                identity = secIdentityRef;
            }
        }
        
        return identity;
    }

    
}
