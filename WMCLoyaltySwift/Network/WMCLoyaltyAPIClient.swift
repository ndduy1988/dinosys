//
//  WMCLoyaltyAPIClient.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


class WMCLoyaltyAPIClient {
    static let sharedInstance = WMCLoyaltyAPIClient()
    static let sslCert = "certificate"
    static let pinningCert = "app"
    var authenticationToken:String?
    
    var defaultManager: Alamofire.SessionManager
    var delegate: Alamofire.SessionDelegate

    init() {
        defaultManager = WMCLoyaltyAPIClient.initializeDefaultManager()
        delegate = defaultManager.delegate
        
        delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?

            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate {
                disposition = .useCredential
                credential = CredentialUtils.credentials()
            }
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = .useCredential
                credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            }
            return (disposition, credential)
        }
    }
    
    static func initializeDefaultManager() -> Alamofire.SessionManager {
        let serverTrustPolicies: [String: ServerTrustPolicy]
            
        if NetworkConstant.isPinning {
            let slCert: SecCertificate? = self.getCertificate(certName: sslCert, certExtension:  "der")
            let pinCert: SecCertificate? = self.getCertificate(certName: pinningCert, certExtension: "cer")

            let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
                // Getting the certificate from the certificate data
                certificates: ServerTrustPolicy.certificates(),
                // Choose to validate the complete certificate chain, not only the certificate itself
                validateCertificateChain: true,
                // Check that the certificate mathes the host who provided it
                validateHost: true
            )
            serverTrustPolicies = [
                "api.dinosys.vn": serverTrustPolicy,
                "192.168.1.88": serverTrustPolicy
            ]
                        
        }else{
            serverTrustPolicies = [
                "api.dinosys.vn": .disableEvaluation,
                "192.168.1.88": .disableEvaluation
            ]
        }
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15
        configuration.timeoutIntervalForResource = 15
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        return Alamofire.SessionManager(configuration: configuration, serverTrustPolicyManager: nil)
        
    }
    
    static func getPubKey() -> SecKey? {
        let certificate = self.getCertificate(certName: sslCert, certExtension:  "cer")
        
        var trust: SecTrust?
        
        let policy = SecPolicyCreateBasicX509()
        let status = SecTrustCreateWithCertificates(certificate!, policy, &trust)
        
        var key: SecKey?
        if status == errSecSuccess {
            key = SecTrustCopyPublicKey(trust!)!;
            print("NetworkImplementation :: getPubKey :: success")
        }
        return key
    }
    
    static func getCertificate(certName: String, certExtension: String) -> SecCertificate? {
        let pathToCert = Bundle.main.path(forResource: certName, ofType: certExtension)
        let certificateData:NSData = NSData(contentsOfFile: pathToCert!)!
        let certificate = SecCertificateCreateWithData(nil, certificateData)
        return certificate
    }

   
    func refreshDefaultManager() {
        self.defaultManager = WMCLoyaltyAPIClient.initializeDefaultManager()
    }
    
    func getAuthenHeader() -> [String : String] {
        return ["Content-Type": "application/json"]
    }
    
    func getHeader() -> [String : String] {
        var headers:[String : String]
        
        authenticationToken = Store.sharedStore.user?.accessToken
        
        if let authentication:String = authenticationToken {
            headers = [
                NetworkConstant.HeaderKey.Authentication: authentication,
                "Content-Type": "application/json"
            ]
        } else {
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        if Store.sharedStore.user?.expiresToken < currentTimeMillis() && Store.sharedStore.user?.accessToken != nil {
            
            WMCLoyaltyLoginRequest.init(callback: { (data:AuthenticationResponse?, error:ErrorResponse?) -> Void in
                
                print("====================REFRESH TOKEN REFRESH TOKEN REFRESH TOKEN REFRESH TOKEN REFRESH TOKEN====================")
                self.authenticationToken = Store.sharedStore.user?.accessToken
                
                if let authentication:String = self.authenticationToken {
                    headers = [
                        NetworkConstant.HeaderKey.Authentication: authentication,
                        "Content-Type": "application/json"
                    ]
                } else {
                    headers = [
                        "Content-Type": "application/json"
                    ]
                }
                
                }, username: Store.sharedStore.user!.username, password: Store.sharedStore.user!.password).execute()
        }
        
        //        if let deviceToken:String = NSUserDefaults.standardUserDefaults().objectForKey(NotificationConstants.PushNotification.kAPNS) as? String {
        //            headers["device_token"] = deviceToken
        //            headers["platform"] = "iOS"
        //        }
        
        return headers
    }
    
    func currentTimeMillis() -> CLong {
        let nowDouble = Date().timeIntervalSince1970
        return CLong(nowDouble*1000)
    }
}
