//
//  WMCLoyaltyProfileImageUploadRequest.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 12/17/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyProfileImageUploadRequestCallback = (ErrorResponse?) -> Void

class WMCLoyaltyProfileImageUploadRequest: APIRequest {
    var loyaltyNumber:Int
    var imageBase64:String
    var imageExtension:String
    var callback:WMCLoyaltyProfileImageUploadRequestCallback
    
    init(callback:@escaping WMCLoyaltyProfileImageUploadRequestCallback, loyaltyNumber:Int, imageBase64:String, imageExtension:String) {
        self.loyaltyNumber = loyaltyNumber
        self.imageBase64 = imageBase64
        self.imageExtension = imageExtension
        self.callback = callback
    }
    
    override func getRequestParams() -> [String : Any]? {
        return [
            "loyaltyNumber" : self.loyaltyNumber as Any,
            "imageBase64" : self.imageBase64 as Any,
            "imageExtension" : self.imageExtension as Any
        ]
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(_):
                self.callback(nil)
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(errorResponse)
                } else{
                    self.callback(nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(.POST, NetworkConstant.getFullPath(NetworkConstant.APIPath.ImageUpload), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(NetworkConstant.APIPath.ImageUpload), method: .post, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}
