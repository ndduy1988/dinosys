//
//  WMCLoyaltyForgotPasswordRequest.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 9/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyForgotPasswordRequestCallback = (ErrorResponse?) -> Void

class WMCLoyaltyForgotPasswordRequest: APIRequest {
    var email:String
    var callback:WMCLoyaltyForgotPasswordRequestCallback
    
    init(callback:@escaping WMCLoyaltyForgotPasswordRequestCallback, email:String) {
        self.email = email
        self.callback = callback
    }
    
    override func getRequestParams() -> [String : Any]? {
        return ["email" : self.email as Any]
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(_):
                self.callback(nil)
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(errorResponse)
                } else{
                    self.callback(nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(.POST, NetworkConstant.getFullPath(NetworkConstant.APIPath.ForgotPassword), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(NetworkConstant.APIPath.ForgotPassword), method: .post, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}
