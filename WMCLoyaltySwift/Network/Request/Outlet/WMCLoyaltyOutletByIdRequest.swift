//
//  WMCLoyaltyOutletByIdRequest.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/11/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyOutletByIdRequestCallback = (OutletResponse?, ErrorResponse?) -> Void

class WMCLoyaltyOutletByIdRequest: APIRequest {
    var callback:WMCLoyaltyOutletByIdRequestCallback
    var path:String
    
    init(callback:@escaping WMCLoyaltyOutletByIdRequestCallback, id:Int) {
        self.callback = callback
        self.path = NetworkConstant.APIPath.OutletById(id)
    }
    
    override func getRequestParams() -> [String : Any]? {
        return nil
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(let JSON):
                let outlet: OutletResponse? = Mapper<OutletResponse>().map(JSONObject: JSON)
                self.callback(outlet, nil)
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(nil, errorResponse)
                } else {
                    self.callback(nil, nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = Alamofire.request(.GET, NetworkConstant.getFullPath(path), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(path), method: .get, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}
