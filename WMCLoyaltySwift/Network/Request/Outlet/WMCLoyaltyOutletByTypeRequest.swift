//
//  WMCLoyaltyOutletByTypeRequest.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/4/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyOutletByTypeRequestCallback = ([OutletResponse]?, PaginationResponse?, ErrorResponse?) -> Void

class WMCLoyaltyOutletByTypeRequest: APIRequest {
    var callback:WMCLoyaltyOutletByTypeRequestCallback
    var page:Int
    var path:String
    
    init(callback:@escaping WMCLoyaltyOutletByTypeRequestCallback, page:Int, type:EnumConstant.Outlet.OutletType) {
        self.callback = callback
        self.page = page
//        self.path = NetworkConstant.APIPath.OutletByType(type)
        self.path = "/outlets"
    }
    
    override func getRequestParams() -> [String : Any]? {
        return nil
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(let JSON):
                if let json = response.result.value as? [String: AnyObject] {
                    let list: [OutletResponse]? = Mapper<OutletResponse>().mapArray(JSONArray: json["content"] as! [[String : Any]])
                    let pagination : PaginationResponse? = Mapper<PaginationResponse>().map(JSONObject: JSON)
                    self.callback(list, pagination, nil)
                } else {
                    self.callback(nil, nil, nil)
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(nil, nil, errorResponse)
                } else {
                    self.callback(nil, nil, nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = Alamofire.request(.GET, NetworkConstant.getFullPath(path), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(path), method: .get, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}
