//
//  WMCLoyaltyTransactionRequest.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/16/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyTransactionRequestCallback = ([TransactionResponse]?, ErrorResponse?) -> Void

class WMCLoyaltyTransactionRequest: APIRequest {
    var callback:WMCLoyaltyTransactionRequestCallback
    
    init(callback:@escaping WMCLoyaltyTransactionRequestCallback) {
        self.callback = callback
    }
    
    override func getRequestParams() -> [String : Any]? {
        return nil
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(_):
                if let json = response.result.value as? [String: AnyObject] {
                    let list: [TransactionResponse]? = Mapper<TransactionResponse>().mapArray(JSONArray: json["content"] as! [[String : Any]])
                    self.callback(list, nil)
                } else {
                    self.callback(nil, nil)
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(nil, errorResponse)
                } else{
                    self.callback(nil, nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(.GET, NetworkConstant.getFullPath(NetworkConstant.APIPath.Transaction), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(NetworkConstant.APIPath.Transaction), method: .get, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}


