//
//  WMCLoyaltyLoginRequest.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyLoginRequestCallback = (AuthenticationResponse?, ErrorResponse?) -> Void

class WMCLoyaltyLoginRequest: APIRequest {
    var username:String?
    var password:String?
    var callback:WMCLoyaltyLoginRequestCallback
    
    init(callback:@escaping WMCLoyaltyLoginRequestCallback, username:String, password:String) {
        self.username = username
        self.password = password
        self.callback = callback
        Store.sharedStore.logout()
    }
    
    override func getRequestParams() -> [String : Any]? {
        return [
            "username" : self.username! as AnyObject,
            "password" : self.password! as AnyObject
        ]
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(let JSON):
                let authenticationResponse = Mapper<AuthenticationResponse>().map(JSONObject: JSON)
                if let auth = authenticationResponse?.token {
                    if auth != "" {
                        let user:User = User()
                        user.accessToken = auth
                        
                        user.username = self.username!
                        user.password = self.password!
                        
                        if let expires = authenticationResponse?.expires {
                            user.expiresToken = expires
                        }
                        
                        Store.sharedStore.user = user
                        Store.sharedStore.save()
                        self.callback(authenticationResponse, nil)
                    } else {
                        self.callback(nil, nil)
                    }
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(nil, errorResponse)
                } else {
                    self.callback(nil, nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(.POST, NetworkConstant.getFullPath(NetworkConstant.APIPath.Authenticate), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getAuthenHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(NetworkConstant.APIPath.Authenticate), method: .post, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}
