//
//  WMCLoyaltyMemberRequest.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/16/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyMemberRequestCallback = ([MemberResponse]?, ErrorResponse?) -> Void

class WMCLoyaltyMemberRequest: APIRequest {
    var callback:WMCLoyaltyMemberRequestCallback
    
    init(callback:@escaping WMCLoyaltyMemberRequestCallback) {
        self.callback = callback
    }
    
    override func getRequestParams() -> [String : Any]? {
        return nil
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(let JSON):
                
                let list: [MemberResponse]? = Mapper<MemberResponse>().mapArray(JSONArray: JSON as! [[String : Any]])
                self.callback(list, nil)
                
//                if let content = JSON["content"] {
//                    let list: [MemberResponse]? = Mapper<MemberResponse>().mapArray(content)
//                    self.callback(list, nil)
//                }else{
//                    self.callback(nil, nil)
//                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(nil, errorResponse)
                } else{
                    self.callback(nil, nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(.GET, NetworkConstant.getFullPath(NetworkConstant.APIPath.Member), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(NetworkConstant.APIPath.Member), method: .get, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}


