//
//  WMCLoyaltyOutletRequest.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 9/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

typealias WMCLoyaltyOutletRequestCallback = ([OutletResponse]?, PaginationResponse?, ErrorResponse?) -> Void

class WMCLoyaltyOutletRequest: APIRequest {
    var callback:WMCLoyaltyOutletRequestCallback
    var page:Int
    
    init(callback:@escaping WMCLoyaltyOutletRequestCallback, page:Int) {
        self.callback = callback
        self.page = page
    }
    
    override func getRequestParams() -> [String : Any]? {
        return nil
    }
    
    override func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        return { (response:Alamofire.DataResponse<Any>) -> Void in
            switch response.result {
            case .success(let JSON):
                
                let list: [OutletResponse]? = Mapper<OutletResponse>().mapArray(JSONArray: JSON as! [[String : Any]])
                let pagination : PaginationResponse? = Mapper<PaginationResponse>().map(JSONObject: JSON)
                self.callback(list, pagination, nil)
                
//                if let content = JSON["content"] {
//                    let list: [OutletResponse]? = Mapper<OutletResponse>().mapArray(JSONArray: content)
//                    let pagination : PaginationResponse? = Mapper<PaginationResponse>().map(JSONObject: JSON)
//                    self.callback(list, pagination, nil)
//                } else {
//                    self.callback(nil, nil, nil)
//                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                if let errorResponse:ErrorResponse = self.parseErrorResponse(response.data!) {
                    self.callback(nil, nil, errorResponse)
                } else{
                    self.callback(nil, nil, nil)
                }
            }
            return
        }
    }
    
    override func cancelRequest() {
        self.request?.cancel()
    }
    
    override func execute() {
        //        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(.GET, NetworkConstant.getFullPath(NetworkConstant.APIPath.Outlet), parameters:getRequestParams(), headers:WMCLoyaltyAPIClient.sharedInstance.getHeader(), encoding:.JSON)
        //            .validate()
        //            .responseJSON(completionHandler: getOperationResponseBlock())
        
        self.request = WMCLoyaltyAPIClient.sharedInstance.defaultManager.request(NetworkConstant.getFullPath(NetworkConstant.APIPath.Outlet), method: .get, parameters: getRequestParams(), encoding: JSONEncoding.default, headers: WMCLoyaltyAPIClient.sharedInstance.getHeader()).validate().responseJSON(completionHandler: getOperationResponseBlock())
    }
}
