//
//  APIRequest.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

protocol AServerAPIRequest {
    func getRequestParams() -> [String : Any]?
    func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void
    func cancelRequest()
    func execute()
}

class APIRequest: NSObject, AServerAPIRequest {
    var request: Alamofire.Request?
    
    func getRequestParams() -> [String : Any]? {
        fatalError("You must override getRequestParams in a subclass")
    }
    
    func getOperationResponseBlock () -> (Alamofire.DataResponse<Any>) -> Void {
        fatalError("You must override getOperationResponseBlock in a subclass")
    }
    
    func cancelRequest() {
        fatalError("You must override cancelRequest in a subclass")
    }
    
    func execute() {
        fatalError("You must override execute in a subclass")
    }
    
    func parseErrorResponse(_ jsonData:Data) -> ErrorResponse? {
        do {
            // Try parsing some valid JSON
            let parsed = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments)
            let errorResponse:ErrorResponse = Mapper<ErrorResponse>().map(JSONObject: parsed)!
            return errorResponse
        }
        catch let error as NSError {
            print("A JSON parsing error occurred, here are the details:\n \(error)")
            if error.code == 403 || error.code == 500 {
                //force login
            }
            return nil
        }
    }
}
