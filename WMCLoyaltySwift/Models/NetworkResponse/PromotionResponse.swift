//
//  PromotionResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/27/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class PromotionResponse: Mappable {
    var id:Int?
    var title:String?
    var description:String?
    var startDate:CLong?
    var endDate:CLong?
    var status:String?
    var createdDate:CLong?
    var images:[ImageResponse]?
    var rules: [RuleResponse]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id                  <- map["id"]
        title               <- map["title"]
        description         <- map["description"]
        startDate           <- map["startDate"]
        endDate             <- map["endDate"]
        status              <- map["status"]
        createdDate         <- map["createdDate"]
        images              <- map["images"]
        rules               <- map["rules"]
    }
}
