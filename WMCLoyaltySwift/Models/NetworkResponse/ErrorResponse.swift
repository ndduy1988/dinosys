//
//  ErrorResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 9/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

/*
{
"timestamp": 1444364025413,
"status": 400,
"error": "Bad Request",
"exception": "org.springframework.web.bind.MissingServletRequestParameterException",
"message": "Required String parameter 'username' is not present",
"path": "/api/authenticate"
}
*/

class ErrorResponse: Mappable {
    var timestamp:Int?
    var status:Int?
    var error:String?
    var exception:String?
    var message:String?
    var path:String?

    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {
        timestamp   <- map["timestamp"]
        status      <- map["status"]
        error       <- map["error"]
        exception   <- map["exception"]
        message     <- map["message"]
        path        <- map["path"]

    }
}
