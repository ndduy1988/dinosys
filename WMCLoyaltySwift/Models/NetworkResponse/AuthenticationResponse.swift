//
//  AuthenticationResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class AuthenticationResponse: Mappable {
    var expires:Int?
    var token:String?
    
    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {
        expires <- map["expires"]
        token   <- map["token"]
    }
}
