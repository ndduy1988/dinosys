//
//  RuleResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 9/6/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import ObjectMapper

class RuleResponse: Mappable {
    var title:String?
    var description:String?
    var ruleType:String?
    var discountType:String?
    var percentage:Int?
    var netSaleAtLeast:Int?
    var outlets:[OutletResponse]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title           <- map["title"]
        description     <- map["description"]
        ruleType        <- map["ruleType"]
        discountType    <- map["discountType"]
        percentage      <- map["percentage"]
        netSaleAtLeast  <- map["netSaleAtLeast"]
        outlets         <- map["outlets"]
    }
}
