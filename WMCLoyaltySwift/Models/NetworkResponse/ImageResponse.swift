//
//  ImageResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/27/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class ImageResponse: Mappable {
    var height:Int?
    var id:Int?
    var name:String?
    var src:String?
    var width:Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        height  <- map["height"]
        id      <- map["id"]
        name    <- map["name"]
        src     <- map["src"]
        width   <- map["width"]
    }
}
