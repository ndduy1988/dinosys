//
//  WMCLoyaltyPaginationResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 19/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class PaginationResponse: Mappable {
    var content:[MemberResponse]?
    var first:Bool?
    var last:Bool?
    var number:Int?
    var numberOfElements:Int?
    var size:Int?
    var sort:String?
    var totalElements:Int?
    var totalPages:Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        content             <- map["content"]
        first               <- map["first"]
        last                <- map["last"]
        number              <- map["number"]
        numberOfElements    <- map["numberOfElements"]
        size                <- map["size"]
        sort                <- map["sort"]
        totalElements       <- map["totalElements"]
        totalPages          <- map["totalPages"]
    }
}
