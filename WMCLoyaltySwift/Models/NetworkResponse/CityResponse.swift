//
//  CityResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 12/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class CityResponse: Mappable {
    var country:CountryResponse?
    var identifier:String?
    var lat:Double?
    var lon:Double?
    var name:String?
    var nameVi:String?
    var population:Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        country     <- map["country"]
        identifier  <- map["identifier"]
        lat         <- map["lat"]
        lon         <- map["lon"]
        name        <- map["name"]
        nameVi      <- map["nameVi"]
        population  <- map["population"]
    }
}
