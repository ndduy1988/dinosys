//
//  CountryResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 12/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class CountryResponse: Mappable {
    var identifier:String?
    var lat:Double?
    var lon:Double?
    var name:String?
    var nameVi:String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        identifier  <- map["identifier"]
        lat         <- map["lat"]
        lon         <- map["lon"]
        name        <- map["name"]
        nameVi      <- map["nameVi"]
    }
}
