//
//  AddressResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 12/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class AddressResponse: Mappable {
    var addressType:String?
    var city:CityResponse?
    var country:CountryResponse?
    var district:DistrictResponse?
    var fullAddress:String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        addressType <- map["addressType"]
        city        <- map["city"]
        country     <- map["country"]
        district    <- map["district"]
        fullAddress <- map["fullAddress"]
    }
}
