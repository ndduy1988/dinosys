//
//  MemberResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/16/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class MemberResponse: Mappable {
    var addresses:AddressResponse?
    var annualQualifyingSpent:Double?
    var annualSpent:Double?
    var avatar:ImageResponse?
    var birthday:Date?
    var companyId:Int?
    var createdBy:String?
    var createdDate:Date?
    var firstName:String?
    var fullName:String?
    var gender:String?
    var id:Int?
    var industryId:Int?
    var jobId:Int?
    var lang:String?
    var lastName:String?
    var lastYearSpent:Double?
    var loyaltyNumber:Int?
    var maritalStatus:String?
    var memberType:String?
    var nationality:CountryResponse?
    var nextTierLevel:TierLevelResponse?
    var paidNight:Int?
    var phone:String?
    var prefix:String?
    var status:Bool?
    var tierLevel:TierLevelResponse?
    var totalQualifyingSpent:Double?
    var totalSpent:Double?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        addresses               <- map["addresses"]
        annualQualifyingSpent   <- map["annualQualifyingSpent"]
        annualSpent             <- map["annualSpent"]
        avatar                  <- map["avatar"]
        birthday                <- (map["birthday"], ISO8601DateTransform())
        companyId               <- map["companyId"]
        createdBy               <- map["createdBy"]
        createdDate             <- (map["createdDate"], ISO8601DateTransform())
        firstName               <- map["firstName"]
        fullName                <- map["fullName"]
        gender                  <- map["gender"]
        id                      <- map["id"]
        industryId              <- map["industryId"]
        jobId                   <- map["jobId"]
        lang                    <- map["lang"]
        lastName                <- map["lastName"]
        lastYearSpent           <- map["lastYearSpent"]
        loyaltyNumber           <- map["loyaltyNumber"]
        maritalStatus           <- map["maritalStatus"]
        memberType              <- map["memberType"]
        nationality             <- map["nationality"]
        nextTierLevel           <- map["nextTierLevel"]
        paidNight               <- map["paidNight"]
        phone                   <- map["phone"]
        prefix                  <- map["prefix"]
        status                  <- map["status"]
        tierLevel               <- map["tierLevel"]
        totalQualifyingSpent    <- map["totalQualifyingSpent"]
        totalSpent              <- map["totalSpent"]
    }
}
