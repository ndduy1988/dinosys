//
//  OutletResponse.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 9/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class OutletResponse: Mappable {
    var active:Bool?
    var address:AddressResponse?
    var createdBy:String?
    var createdDate:Date?
    var description:String?
    var email:String?
    var facebook:String?
    var fax:String?
    var foursquare:String?
    var googleplus:String?
    var id:Int?
    var identifier:String?
    var lat:Double?
    var logoUrl:String?
    var lon:Double?
    var name:String?
    var nameVi:String?
    var opening:String?
    var outletCode:String?
    var posterUrl:String?
    var promotions:[PromotionResponse]?
    var rewards:[RewardResponse]?
    var telephone:String?
    var type:String?
    var url:String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        active      <- map["active"]
        address     <- map["address"]
        createdBy   <- map["createdBy"]
        createdDate <- (map["createdDate"], ISO8601DateTransform())
        description <- map["description"]
        email       <- map["email"]
        facebook    <- map["facebook"]
        fax         <- map["fax"]
        foursquare  <- map["foursquare"]
        googleplus  <- map["googleplus"]
        id          <- map["id"]
        identifier  <- map["identifier"]
        lat         <- map["lat"]
        logoUrl     <- map["logoUrl"]
        lon         <- map["lon"]
        name        <- map["name"]
        nameVi      <- map["nameVi"]
        opening     <- map["opening"]
        outletCode  <- map["outletCode"]
        posterUrl   <- map["posterUrl"]
        promotions  <- map["promotions"]
        rewards     <- map["rewards"]
        telephone   <- map["telephone"]
        type        <- map["type"]
        url         <- map["url"]
    }
}
