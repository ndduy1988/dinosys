//
//  TierLevelResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class TierLevelResponse: Mappable {
    var discount:Int?
    var expiryType:String?
    var id:Int?
    var levelKey:String?
    var levelName:String?
    var qualifyingSpent:Double?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        discount        <- map["discount"]
        expiryType      <- map["expiryType"]
        id              <- map["id"]
        levelKey        <- map["levelKey"]
        levelName       <- map["levelName"]
        qualifyingSpent <- map["qualifyingSpent"]
    }
}
