//
//  TransactionResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/16/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class TransactionResponse: Mappable {
    var beverage:Double?
    var discount:Int?
    var endDate:Date?
    var food:Double?
    var fromPaidNight:String?
    var id:Int?
    var liquor:Double?
    var member:MemberResponse?
    var netSale:Double?
    var other:Double?
    var outlet:OutletResponse?
    var pax:Int?
    var payType:String?
    var receiptDate:CLong?
    var remark:String?
    var serviceCharge:Double?
    var sourceTransaction:String?
    var toPaidNight:String?
    var total:Double?
    var transactionNumber:Int?
    var vat:Double?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        beverage            <- map["beverage"]
        discount            <- map["discount"]
        endDate             <- (map["endDate"], ISO8601DateTransform())
        food                <- map["food"]
        fromPaidNight       <- map["fromPaidNight"]
        id                  <- map["id"]
        liquor              <- map["liquor"]
        member              <- map["member"]
        netSale             <- map["netSale"]
        other               <- map["other"]
        outlet              <- map["outlet"]
        pax                 <- map["pax"]
        payType             <- map["payType"]
        receiptDate         <- map["receiptDate"]
        remark              <- map["remark"]
        serviceCharge       <- map["serviceCharge"]
        sourceTransaction   <- map["sourceTransaction"]
        toPaidNight         <- map["toPaidNight"]
        total               <- map["total"]
        transactionNumber   <- map["transactionNumber"]
        vat                 <- map["vat"]
    }
}
