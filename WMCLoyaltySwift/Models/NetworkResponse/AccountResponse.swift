//
//  AccountResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/15/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class AccountResponse: Mappable {
    var addresses:AddressResponse?
    var annualQualifyingSpent:Double?
    var annualSpent:Double?
    var avatarUrl: String?
    var birthday:Date?
    var companyName:String?
    var createdBy:String?
    var createDate:CLong?
    var email:String?
    var firstName:String?
    var gender:String?
    var id:Int?
    var industryName:String?
    var isGeneralMember:Bool?
    var jobName:String?
    var lang:String?
    var lastModifiedBy:String?
    var lastModifiedDate:Date?
    var lastName:String?
    var lastYearSpent:Double?
    var login:String?
    var loyaltyNumber:Int?
    var maritalStatus:String?
    var memberType:String?
    var nationality:CountryResponse?
    var nextLevelSpent:Double?
    var nextTierLevel:TierLevelResponse?
    var nonPickUpAt:Int?
    var nonPickUpDate:Date?
    var paidNight: Int?
    var password:String?
    var phone:String?
    var pickUpAt:Int?
    var pickUpDate:Date?
    var prefix:String?
    var sequenceName:String?
    var tierLevel:TierLevelResponse?
    var totalQualifyingSpent:Double?
    var totalSpent:Double?
    var userId:Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        addresses               <- map["addresses"]
        annualQualifyingSpent   <- map["annualQualifyingSpent"]
        annualSpent             <- map["annualSpent"]
        avatarUrl               <- map["avatarUrl"]
        birthday                <- (map["birthday"], CustomDateFormatTransform(formatString:"yyyy-MM-dd"))
        companyName             <- map["companyName"]
        createdBy               <- map["createdBy"]
        createDate              <- map["createDate"]
        email                   <- map["email"]
        firstName               <- map["firstName"]
        gender                  <- map["gender"]
        id                      <- map["id"]
        industryName            <- map["industryName"]
        isGeneralMember         <- map["isGeneralMember"]
        jobName                 <- map["jobName"]
        lang                    <- map["lang"]
        lastModifiedBy          <- map["lastModifiedBy"]
        lastModifiedDate        <- (map["lastModifiedDate"], CustomDateFormatTransform(formatString:"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
        lastName                <- map["lastName"]
        lastYearSpent           <- map["lastYearSpent"]
        login                   <- map["login"]
        loyaltyNumber           <- map["loyaltyNumber"]
        maritalStatus           <- map["maritalStatus"]
        memberType              <- map["memberType"]
        nationality             <- map["nationality"]
        nextLevelSpent          <- map["nextLevelSpent"]
        nextTierLevel           <- map["nextTierLevel"]
        nonPickUpAt             <- map["nonPickUpAt"]
        nonPickUpDate           <- (map["nonPickUpDate"], ISO8601DateTransform())
        paidNight               <- map["paidNight"]
        password                <- map["password"]
        phone                   <- map["phone"]
        pickUpAt                <- map["pickUpAt"]
        pickUpDate              <- (map["pickUpDate"], ISO8601DateTransform())
        prefix                  <- map["prefix"]
        sequenceName            <- map["sequenceName"]
        tierLevel               <- map["tierLevel"]
        totalQualifyingSpent    <- map["totalQualifyingSpent"]
        totalSpent              <- map["totalSpent"]
        userId                  <- map["userId"]
    }
}
