//
//  RewardResponse.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import ObjectMapper

class RewardResponse: Mappable {
    var description:String?
    var functionName:String?
    var id:Int?
    var isActive:Bool?
    var isGlobal:Bool?
    var name:String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        description     <- map["description"]
        functionName    <- map["functionName"]
        id              <- map["id"]
        isActive        <- map["isActive"]
        isGlobal        <- map["isGlobal"]
        name            <- map["name"]
    }
}
