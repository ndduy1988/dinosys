//
//  Store.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class Store {
    
    let kUserStore:String = "UserStore"
    
    fileprivate var _user:User?
    
    var user:User? {
        get {
            if let user:User = self._user {
                return user
            }
            if let data = UserDefaults.standard.object(forKey: kUserStore) as? Data {
                
                let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
                
                return user
                
            } else {
                return nil
            }
        }
        set (newUser) {
            _user = newUser
        }
    }
    
    static let sharedStore = Store()
    
    init() {}
    
    func save() {
        if ((_user) != nil) {
            if let user = _user {
                let data = NSKeyedArchiver.archivedData(withRootObject: user)
                UserDefaults.standard.set(data, forKey: kUserStore)
                UserDefaults.standard.synchronize()
                
                print("SAVE SAVE SAVE SAVE \(_user?.username)")
            }
        }
    }
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: kUserStore)
        _user = nil
        save()
    }
}
