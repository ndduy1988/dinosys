//
//  User.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    var accessToken:String?
    var expiresToken:CLong = 0
    var username:String = ""
    var password:String = ""
    
    func isLoggedIn() -> Bool {
        if let _ = accessToken {
            return true
        } else {
            return false
        }
    }
    
    required convenience init?(coder decoder: NSCoder) {
        //access token
        guard let accessToken = decoder.decodeObject(forKey: "accessToken") as? String
            else { return nil }
        guard let expiresToken = decoder.decodeObject(forKey: "expiresToken") as? CLong
            else { return nil }
        guard let username = decoder.decodeObject(forKey: "username") as? String
            else { return nil }
        guard let password = decoder.decodeObject(forKey: "password") as? String
            else { return nil }
        
        self.init()
        self.accessToken = accessToken
        self.expiresToken = expiresToken
        self.username = username
        self.password = password
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.accessToken, forKey: "accessToken")
        coder.encode(self.expiresToken, forKey: "expiresToken")
        coder.encode(self.username, forKey: "username")
        coder.encode(self.password, forKey: "password")
    }
}
