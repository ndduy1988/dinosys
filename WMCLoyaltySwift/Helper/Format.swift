//
//  Format.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/12/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class Format {
    static func currency(_ price:String, code:String) -> String {
        let convertPrice = NSNumber(value: Double(price)! as Double)
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = code
        return formatter.string(from: convertPrice)!
    }
    
    //http://www.codingexplorer.com/swiftly-getting-human-readable-date-nsdateformatter/
    static func date(_ date: Date, format: String) -> String {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = format
        return dayTimePeriodFormatter.string(from: date)
    }
    
    static func convertMiliToDate(_ date:CLong) -> String {
        let timeinterval : TimeInterval = Double(date)
        let date = Date(timeIntervalSince1970: timeinterval)
        return dateToString(date)
    }
    
    static func dateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        formatter.timeZone = .current
        return formatter.string(from: date)
    }
}
