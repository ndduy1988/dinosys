//
//  Helper.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/15/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import MapKit

class Helper {
    //LOGIN & FORGOT PASSWORD
    static func setTextFieldBorder(_ textField: UITextField) {
        textField.layer.masksToBounds = true
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.gray.cgColor
    }
    
    static func limitCharacter(_ textField: UITextField, range: NSRange, string: String) -> Bool {
        if (range.length + range.location > textField.text!.characters.count ) {
            return false
        }
        let newLength = textField.text!.characters.count + string.characters.count - range.length
        return newLength <= 50
    }
    
    static func dismissKeyboard(_ userName:UITextField, passWord:UITextField) {
        if userName.isEditing == true {
            userName.resignFirstResponder()
        } else if passWord.isEditing == true {
            passWord.resignFirstResponder()
        }
    }
    
    static func isValidEmail(_ str:String) -> Bool {
        return NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}").evaluate(with: str)
    }
    
    static func isValidLoyaltyNumber(_ str:String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", "^\\d{12}$").evaluate(with: str)
    }
    
    //MAP
    static func fitAnnotationOnMap(_ routeMapView: MKMapView, scale:Double) {
        var zoomRect: MKMapRect = MKMapRectNull
        for annotation: MKAnnotation in routeMapView.annotations {
            let annotationPoint: MKMapPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect: MKMapRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.01, 0.01)
            zoomRect = MKMapRectUnion(zoomRect, pointRect)
        }
        
        let insetWidth: Double = -zoomRect.size.width * scale
        let insetHeight: Double = -zoomRect.size.height * scale
        let insetRect: MKMapRect = MKMapRectInset(zoomRect, insetWidth, insetHeight)
        routeMapView.setVisibleMapRect(insetRect, animated: true)
    }
    
    //ANIMATION TABLE CELL
    static func AnimateTable(_ tableView:UITableView) {
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableHeight: CGFloat = tableView.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for cell in cells {
            let cell: UITableViewCell = cell as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 3.0, initialSpringVelocity: 0, options: UIViewAnimationOptions.allowUserInteraction, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
                }, completion: nil)
            index += 1
        }
    }
}
