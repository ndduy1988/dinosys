//
//  Alert.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/8/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

open class Alert: NSObject {
    static func checkConnection(_ delegate: UIViewController) {
        let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            exit(0)
        }))
        delegate.present(alert, animated: true, completion: nil)
    }
    static func signOut(_ delegate: UIViewController, message: String) {
        let alert = UIAlertController(title: "Sign Out", message: message, preferredStyle: UIAlertControllerStyle.alert)
        delegate.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Sign Out", style: .destructive, handler: { action in
            Store.sharedStore.logout()
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.MainMenuNavigationController)
        }))
    }
    
    static func hostOption(_ delegate: UIViewController, message: String) {
        let alert = UIAlertController(title: "Host", message: message, preferredStyle: UIAlertControllerStyle.alert)
        delegate.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Stable", style: .default, handler: { action in
            NetworkConstant.BaseURL = "http://192.168.1.88"
        }))
        alert.addAction(UIAlertAction(title: "Test", style: .default, handler: { action in
            NetworkConstant.BaseURL = "http://192.168.1.88:8081"
        }))
        alert.addAction(UIAlertAction(title: "Pinning", style: .default, handler: { action in
            NetworkConstant.BaseURL = "https://192.168.1.88:8081"
            NetworkConstant.isPinning = true
            WMCLoyaltyAPIClient.sharedInstance.refreshDefaultManager()
        }))
    }
}
