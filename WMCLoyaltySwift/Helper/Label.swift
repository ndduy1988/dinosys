//
//  Label.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/13/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class Label {
    static func height(_ text:String, font:UIFont, width:CGFloat, insets: UIEdgeInsets) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width - insets.left - insets.right, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height + insets.top + insets.bottom
    }
    
    static func calculateHeight(_ string:String?, screenWidth:CGFloat, lines:Int, fontHeight:CGFloat, cellHeight:CGFloat) -> CGFloat {
        if let str = string {
            if str.isEmpty {
                return 0
            } else {
                return optimize(height(str, font: UIFont(name:"MyriadPro-SemiExt", size: 14)!, width: screenWidth, insets:UIEdgeInsetsMake(0, 0, 0, 0)), lines:lines, fontHeight:fontHeight, cellHeight:cellHeight)
            }
        }
        return 0
    }
    
    static func optimize(_ height:CGFloat, lines:Int, fontHeight:CGFloat, cellHeight:CGFloat) -> CGFloat {
        for i in 1...lines {
        //for var i = 1; i <= lines; i += 1 {
            if height <= fontHeight {
                return cellHeight
            } else if height <= fontHeight * CGFloat(i) {
                return cellHeight + (fontHeight * CGFloat(i))
            } else if height >= fontHeight * CGFloat(lines - 1) {
                return cellHeight + (fontHeight * CGFloat(lines - 1))
            }
        }
        return 0
    }
}
