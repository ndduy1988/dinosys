//
//  RuleDetailTableViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 9/6/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RuleDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var infoView: UIView!
}
