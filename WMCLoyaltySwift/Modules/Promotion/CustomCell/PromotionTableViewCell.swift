//
//  PromotionTableViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/8/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class PromotionTableViewCell: UITableViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var infoView: UIView!
}


