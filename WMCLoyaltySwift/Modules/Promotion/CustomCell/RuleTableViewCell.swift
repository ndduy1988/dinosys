//
//  RuleTableViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 9/6/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RuleTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var ruleName: UILabel!
}
