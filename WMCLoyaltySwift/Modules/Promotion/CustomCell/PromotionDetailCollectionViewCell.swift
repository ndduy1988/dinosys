//
//  PromotionDetailCollectionViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 12/28/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class PromotionDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
