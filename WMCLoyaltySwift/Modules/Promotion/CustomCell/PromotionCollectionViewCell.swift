//
//  PromotionCollectionViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/12/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class PromotionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var labelPercentage: UILabel!
    
    override func awakeFromNib() {
        imageView.layer.cornerRadius = 2
        imageView.clipsToBounds = true
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowRadius = 2
        layer.shadowOpacity = 1
    }
}
