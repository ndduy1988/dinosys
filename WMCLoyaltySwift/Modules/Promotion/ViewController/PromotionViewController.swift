//
//  PromotionViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/7/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView

class PromotionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var promotionTableView: UITableView!
    @IBOutlet weak var promotionSearchBar: UISearchBar!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    var activityIndicatorView: NVActivityIndicatorView? = nil
    
    //collapse
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var buttonUndo: UIButton!
    @IBOutlet weak var labelPromotion: UILabel!
    
    var viewModel: PromotionViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    
    //search properties
    var filtered: [PromotionResponse] = []
    var isSearching: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initActivityIndicator()
        customPullToRequest()
        initFirst()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.campaign_list_screen)
    }
    
    func trackEvent(_ value: Int) {
        //track.trackEvent(TrackingConstant.Screen.campaign_list_screen, action: TrackingConstant.Action.campaign_select, label: "", value: value)
    }
    
    func initCorner() {
        infoView.layer.cornerRadius = 4
        infoView.clipsToBounds = true
    }
    
    func customPullToRequest() {
        if let customSubview = Bundle.main.loadNibNamed("CustomSubview", owner: self, options: nil)?.first as? CustomSubview {
            promotionTableView.addPullToRefreshWithAction({
                OperationQueue().addOperation {
                    sleep(1)
                    OperationQueue.main.addOperation {
                        self.initFirst()
                        self.promotionTableView.stopPullToRefresh()
                    }
                }
                }, withAnimator: customSubview)
        }
    }
    
    func initFirst() {
        viewModel = PromotionViewModel(reloadTableViewCallback: reloadTableView)
        if let model = viewModel {
            model.delegate = self
            model.checkConnection()
        }
    }
    
    func reloadTableView() {
        if let tv = promotionTableView {
            tv.reloadData()
            if let indicator = activityIndicatorView {
                indicator.stopAnimating()
            }
        }
    }
    
    ///////////////////////////////////AUTO HIDE NAVIGATION BAR///////////////////////////////////
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
        activityIndicatorView!.startAnimating()
        reloadTableView()
        reloadSetting()
    }
    
    func reloadSetting() {
        if let model = viewModel {
            model.switchSearch(1, searchBar: 0, btnSearch: 1, btnUndo: 0)
        }
        Helper.AnimateTable(promotionTableView)
        promotionSearchBar.text = ""
    }
    
    ///////////////////////////////////TABLE VIEW///////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filtered.count
        } else if let model = viewModel {
            if let array = model.dataArray {
                lblNoData.isHidden = showEmptyTableErrorMessage()
                return array.count
            }
        }
        lblNoData.isHidden = showEmptyTableErrorMessage()
        return 0
    }
    
    func showEmptyTableErrorMessage() -> Bool {
        if let model = viewModel {
            if let array = model.dataArray {
                if array.count <= 0 {
                    return false
                }
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellConstant.TableCellIdentifier.promotion, for: indexPath) as! PromotionTableViewCell
        
        cell.infoView.layer.cornerRadius = 4
        cell.infoView.clipsToBounds = true
        
        if isSearching {
            if let image = filtered[(indexPath as NSIndexPath).row].images {
                if !image.isEmpty {
                    if let src = image[0].src {
                        if !src.isEmpty {
                            cell.imageCell.contentMode = .scaleAspectFill
                            cell.imageCell.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(src))!)
                        } else {
                            cell.imageCell.contentMode = .scaleAspectFill
                            cell.imageCell.image = UIImage(named: "empty_image")
                        }
                    } else {
                        cell.imageCell.contentMode = .scaleAspectFill
                        cell.imageCell.image = UIImage(named: "empty_image")
                    }
                } else {
                    cell.imageCell.contentMode = .scaleAspectFill
                    cell.imageCell.image = UIImage(named: "empty_image")
                }
            } else {
                cell.imageCell.contentMode = .scaleAspectFill
                cell.imageCell.image = UIImage(named: "empty_image")
            }
            
            if let title = filtered[(indexPath as NSIndexPath).row].title {
                if !title.isEmpty {
                    cell.labelTitle.text = title
                }
            }
            
            if let end = filtered[(indexPath as NSIndexPath).row].endDate {
                cell.labelEndDate.text = "End " + Format.convertMiliToDate(end)
            }
        } else {
            if let model = viewModel {
                if let array = model.dataArray {
                    if let image = array[(indexPath as NSIndexPath).row].images {
                        if !image.isEmpty {
                            if let src = image[0].src {
                                if !src.isEmpty {
                                    cell.imageCell.contentMode = .scaleAspectFill
                                    cell.imageCell.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(src))!)
                                } else {
                                    cell.imageCell.contentMode = .scaleAspectFill
                                    cell.imageCell.image = UIImage(named: "empty_image")
                                }
                            } else {
                                cell.imageCell.contentMode = .scaleAspectFill
                                cell.imageCell.image = UIImage(named: "empty_image")
                            }
                        } else {
                            cell.imageCell.contentMode = .scaleAspectFill
                            cell.imageCell.image = UIImage(named: "empty_image")
                        }
                    } else {
                        cell.imageCell.contentMode = .scaleAspectFill
                        cell.imageCell.image = UIImage(named: "empty_image")
                    }
                    
                    if let title = array[(indexPath as NSIndexPath).row].title {
                        if !title.isEmpty {
                            cell.labelTitle.text = title
                        }
                    }
                    
                    if let end = array[(indexPath as NSIndexPath).row].endDate {
                        cell.labelEndDate.text = "End " + Format.convertMiliToDate(end)
                    }
                }
            }
        }
        
        if let model = viewModel {
            if let array = model.dataArray {
                if ((indexPath as NSIndexPath).row == array.count - 1) {
                    if let pagination = model.paginationResponse {
                        if ((pagination.last) != nil && pagination.last == false) {
                            model.reloadContent()
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = viewModel {
            model.animateViewMoving(false)
        }
        promotionSearchBar.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.Segue.promotionDetailContainer {
            if let model = viewModel {
                if let array = model.dataArray,
                    let row = (promotionTableView.indexPathForSelectedRow as NSIndexPath?)?.row,
                    let vc = segue.destination as? PromotionDetailContainerViewController {
                    vc.passedObject = array[row]
                    trackEvent(row)
                }
            }
        }
    }
    
    ///////////////////////////////////SEARCH BAR///////////////////////////////////
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text!.isEmpty {
            isSearching = false
        } else {
            isSearching = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        resetSearch(false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.endEditing(true)
    }
    
    func resetSearch(_ isTapOut:Bool) {
        isSearching = false
        filtered.removeAll()
        if isTapOut {
            promotionSearchBar.text = ""
            promotionSearchBar.endEditing(true)
            reloadTableView()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let model = viewModel {
            if let array = model.dataArray {
                filtered = array.filter({ (text) -> Bool in
                    let tmp: NSString = text.title! as NSString
                    let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                    return range.location != NSNotFound
                })
            }
        }
        
        if filtered.count == 0 {
            isSearching = false
        } else {
            isSearching = true
        }
        reloadTableView()
    }
    
    @IBAction func dissmissKeyboardOnTap(_ sender: AnyObject) {
        resetSearch(true)
    }
    
    @IBAction func onSearchPressed(_ sender: AnyObject) {
        if let model = viewModel {
            model.animateViewMoving(true)
            promotionSearchBar.becomeFirstResponder()
        }
    }
    
    @IBAction func onUndoPressed(_ sender: AnyObject) {
        if let model = viewModel {
            model.animateViewMoving(false)
            reloadUndoPressed()
        }
    }
    
    func reloadUndoPressed() {
        isSearching = false
        promotionSearchBar.text = ""
        promotionSearchBar.resignFirstResponder()
        reloadTableView()
    }
    
    func initActivityIndicator() {
        let activityTypes: [NVActivityIndicatorType] = [
            .ballPulse,
            .ballGridPulse,
            .ballClipRotate,
            .ballClipRotatePulse,
            .squareSpin,
            .ballClipRotateMultiple,
            .ballPulseRise,
            .ballRotate,
            .cubeTransition,
            .ballZigZag,
            .ballZigZagDeflect,
            .ballTrianglePath,
            .ballScale,
            .lineScale,
            .lineScaleParty,
            .ballScaleMultiple,
            .ballPulseSync,
            .ballBeat,
            .lineScalePulseOut,
            .lineScalePulseOutRapid,
            .ballScaleRipple,
            .ballScaleRippleMultiple,
            .ballSpinFadeLoader,
            .lineSpinFadeLoader,
            .triangleSkewSpin,
            .pacman,
            .ballGridBeat,
            .semiCircleSpin,
            .ballRotateChase]
        
        let frame = CGRect(x: self.view.frame.width/2 - (self.view.frame.width/12)/2, y: self.view.frame.height/3, width: self.view.frame.width/12, height: self.view.frame.width/12)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: activityTypes[13])
        self.view.addSubview(activityIndicatorView!)
    }
}
