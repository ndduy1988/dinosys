//
//  RuleDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 9/6/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RuleDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var passedObject: [OutletResponse]?
    @IBOutlet weak var infoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
    }
    
    func initCorner() {
        infoView.layer.cornerRadius = 4
        infoView.clipsToBounds = true
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countOutlets()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellConstant.TableCellIdentifier.rulesDetail, for: indexPath) as! RuleDetailTableViewCell
        
        cell.infoView.layer.cornerRadius = 4
        cell.infoView.clipsToBounds = true
        
        if let object = passedObject {
            if let name = object[(indexPath as NSIndexPath).row].name {
                if !name.isEmpty {
                    cell.labelName.text = name
                }
            }
            
            if let logo = object[(indexPath as NSIndexPath).row].logoUrl {
                if !logo.isEmpty {
                    cell.imageCell.contentMode = .scaleAspectFit
                    cell.imageCell.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(logo))!)
                }
            }
            
            if let address = object[(indexPath as NSIndexPath).row].address {
                if let fullAddress = address.fullAddress {
                    if !fullAddress.isEmpty {
                        cell.labelAddress.text = fullAddress
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let object = passedObject {
            if let nav = navigationController {
                if object[(indexPath as NSIndexPath).row].type == "restaurant" {
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.RestaurantDetailContainerViewController) as? RestaurantDetailContainerViewController {
                        vc.passedObject = object[(indexPath as NSIndexPath).row]
                        nav.pushViewController(vc, animated: true)
                    }
                } else if object[(indexPath as NSIndexPath).row].type == "hotel" {
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.HotelDetailContainerViewController) as? HotelDetailContainerViewController {
                        vc.passedObject = object[(indexPath as NSIndexPath).row]
                        nav.pushViewController(vc, animated: true)
                    }
                } else {
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.RestaurantDetailContainerViewController) as? RestaurantDetailContainerViewController {
                        vc.passedObject = object[(indexPath as NSIndexPath).row]
                        nav.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func countOutlets() -> Int {
        var count = 0
        if let object = passedObject {
            if !object.isEmpty {
                for i in 0 ..< object.count {
                    if let name = object[i].name {
                        if !name.isEmpty {
                            count += 1
                        }
                    }
                }
            }
        }
        return count
    }
}
