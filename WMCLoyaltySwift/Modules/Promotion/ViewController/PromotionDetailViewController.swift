//
//  PromotionDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/9/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class PromotionDetailViewController: UITableViewController {
    
    var passedObject: PromotionResponse?
    var viewModel: PromotionDetailViewModel?
    
    @IBOutlet weak var cellDate: UITableViewCell!
    @IBOutlet weak var cellStatus: UITableViewCell!
    @IBOutlet weak var cellDescription: UITableViewCell!
    
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelStartDate: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    
 
    @IBOutlet weak var labelDescription: UILabel!
    
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    func initFirst() {
        viewModel = PromotionDetailViewModel()
        if let model = viewModel {
            model.delegate = self
            model.showDetail()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.campaign_detail_screen)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).row == 0 {
            return cellDate.frame.size.height
        } else if (indexPath as NSIndexPath).row == 1 {
            if let status = passedObject?.status {
                if !status.isEmpty {
                    return cellStatus.frame.size.height
                }
            } else {
                cellStatus.isHidden = true
            }
        } else if (indexPath as NSIndexPath).row == 2 {
            if Label.calculateHeight(passedObject?.description, screenWidth: self.view.frame.size.width, lines: 1, fontHeight: 14, cellHeight: 53) == 0 {
                cellDescription.isHidden = true
            } else {
                
                return Label.calculateHeight(passedObject?.description, screenWidth: self.view.frame.size.width, lines: 5, fontHeight: 14, cellHeight: 53)
            }
        }
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.EmbedSegue.rules {
            if let vc = segue.destination as? RuleViewController {
                vc.passedObject = passedObject
            }
        }
    }
}
