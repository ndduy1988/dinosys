//
//  RuleViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 9/6/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RuleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var passedObject: PromotionResponse?
    @IBOutlet weak var rulesTableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      changeHeightContainer()
    }
    
    func changeHeightContainer() {
        if Label.calculateHeight(passedObject?.description, screenWidth: self.view.frame.size.width, lines: 5, fontHeight: 14, cellHeight: 53) == 0 {
            containerView.frame.size.height = 106
        } else if Label.calculateHeight(passedObject?.description, screenWidth: self.view.frame.size.width, lines: 5, fontHeight: 14, cellHeight: 53) == 53 {
            containerView.frame.size.height = 159
        } else if Label.calculateHeight(passedObject?.description, screenWidth: self.view.frame.size.width, lines: 5, fontHeight: 14, cellHeight: 53) == 81 {
            containerView.frame.size.height = 187
        } else if Label.calculateHeight(passedObject?.description, screenWidth: self.view.frame.size.width, lines: 5, fontHeight: 14, cellHeight: 53) == 95 {
            containerView.frame.size.height = 201
        }
    }
    
    //    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if let object = passedObject {
    //            if let rule = object.rules {
    //                if !rule.isEmpty {
    //                    return rule[section].title
    //                }
    //            }
    //        }
    //        return 1
    //    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countRules()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellConstant.TableCellIdentifier.rules, for: indexPath) as! RuleTableViewCell
        
        //        if let object = passedObject {
        //            if let rule = object.rules {
        //                if let outlet = rule[indexPath.section].outlets {
        //                    if let name = outlet[indexPath.row].name {
        //                        cell.labelTitle.text = name
        //                    }
        //                }
        //            }
        //        }
        
        if let object = passedObject {
            if let rule = object.rules {
                cell.labelTitle.text = rule[(indexPath as NSIndexPath).row].title
            }
        }
        
        if (indexPath as NSIndexPath).row > 0 {
            cell.ruleName.isHidden = true
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.Segue.rulesDetail {
            if let vc = segue.destination as? RuleDetailViewController {
                if let row = (rulesTableView.indexPathForSelectedRow as NSIndexPath?)?.row {
                    if let object = passedObject {
                        if let rule = object.rules {
                            if let outlet = rule[row].outlets {
                                vc.passedObject = outlet
                                  changeHeightContainer()
                            }
                        }
                    }
                }
            }
        } else if segue.identifier == SegueConstant.EmbedSegue.promotionDetail {
            if let vc = segue.destination as? PromotionDetailViewController {
                vc.passedObject = passedObject
            }
        }
    }
    
    func countRules() -> Int {
        var count = 0
        if let object = passedObject {
            if let rule = object.rules {
                if !rule.isEmpty {
                    for i in 0 ..< rule.count {
                        if let name = rule[i].title {
                            if !name.isEmpty {
                                count += 1
                            }
                        }
                    }
                }
            }
        }
        return count
    }
    
    func countOutlets(_ value:Int) -> Int {
        var count = 0
        if let object = passedObject {
            if let rule = object.rules {
                if !rule.isEmpty {
                    if let outlet = rule[value].outlets {
                        for i in 0 ..<  outlet.count {
                            if let name = outlet[i].name {
                                if !name.isEmpty {
                                    count += 1
                                }
                            }
                        }
                    }
                }
            }
        }
        return count
    }
}
