//
//  PromotionDetailContainerViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/22/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class PromotionDetailContainerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var passedObject: PromotionResponse?
    var viewModel: PromotionDetailContainerViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    func initFirst() {
        viewModel = PromotionDetailContainerViewModel()
        if let model = viewModel {
            model.delegate = self
            model.showDetail()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(collectionView.contentOffset.x / self.view.frame.size.width)
        //track.trackEvent(TrackingConstant.Screen.campaign_detail_screen, action: TrackingConstant.Action.swipe_campaign_image, label: "", value: pageControl.currentPage)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let model = viewModel {
            if model.countImage() > 0 {
                pageControl.numberOfPages = model.countImage()
                return model.countImage()
            }
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellConstant.CollectionCellIdentifier.promotionDetail, for: indexPath) as! PromotionDetailCollectionViewCell
        initCorner(cell)
        
        if let object = self.passedObject {
            if let image = object.images {
                if !image.isEmpty {
                    if let src = image[(indexPath as NSIndexPath).row].src {
                        if !src.isEmpty {
                            if image.count > 0 {
                                cell.imageView.contentMode = .scaleAspectFill
                                cell.imageView.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(src))!)
                            }
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.EmbedSegue.rules {
            if let vc = segue.destination as? RuleViewController {
                vc.passedObject = passedObject
            }
        }
    }
    
    func initCorner(_ cell: PromotionDetailCollectionViewCell) {
        cell.imageView.layer.cornerRadius = 4
        cell.imageView.clipsToBounds = true
    }
}
