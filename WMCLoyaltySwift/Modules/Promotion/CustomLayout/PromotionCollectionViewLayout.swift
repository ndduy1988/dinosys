//
//  PromotionCollectionViewLayout.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/12/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


class PromotionCollectionViewLayout: UICollectionViewFlowLayout {
    var itemWidth: CGFloat = 215
    var itemHeight: CGFloat = 247
    let itemSpacing: CGFloat = 15
    var maxXPos: CGFloat = 0
    var layoutInfo: [IndexPath:UICollectionViewLayoutAttributes] = [IndexPath:UICollectionViewLayoutAttributes]()
    
    override init() {
        super.init()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        updateItemSize()
        self.minimumInteritemSpacing = itemSpacing
        self.minimumLineSpacing = itemSpacing
        self.scrollDirection = UICollectionViewScrollDirection.horizontal
    }
    
    //ratio 1.2
    func updateItemSize() {
        if DeviceType.IS_IPHONE_5 { //320
            itemWidth = 215
            itemHeight = 258
        } else if DeviceType.IS_IPHONE_6 { //375
            itemWidth = 260
            itemHeight = 312
        } else if DeviceType.IS_IPHONE_6P { //414
            itemWidth = 296
            itemHeight = 355
        }
        self.itemSize = CGSize(width: itemWidth, height: itemHeight)
    }
    
    override func prepare() {
        layoutInfo = [IndexPath:UICollectionViewLayoutAttributes]()
        
        for i in 0..<self.collectionView!.numberOfItems(inSection: 0) {
            //for var i = 0; i < self.collectionView?.numberOfItems(inSection: 0); i += 1 {
            let indexPath = IndexPath(row: i, section: 0)
            let itemAttributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            itemAttributes.frame = frameForItemAtIndexPath(indexPath, count:i)
            if itemAttributes.frame.origin.x > maxXPos {
                maxXPos = itemAttributes.frame.origin.x
            }
            layoutInfo[indexPath] = itemAttributes
        }
    }
    
    func frameForItemAtIndexPath(_ indexPath: IndexPath, count:Int) -> CGRect {
        let xPos = (itemWidth + self.minimumInteritemSpacing) * CGFloat(count)
        let yPos = (itemHeight + self.minimumLineSpacing)
        let rect: CGRect = CGRect(x: xPos, y: yPos, width: itemWidth, height: itemHeight)
        return rect
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return layoutInfo[indexPath]
    }
    
    //from WWDC video
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let array: Array = super.layoutAttributesForElements(in: rect)!
        var visibleRect = CGRect.zero
        visibleRect.origin = (self.collectionView?.contentOffset)!
        visibleRect.size = (self.collectionView?.bounds.size)!
        
        for attributes: UICollectionViewLayoutAttributes in array {
            if attributes.frame.intersects(rect) {
                let distance: CGFloat = visibleRect.midX - attributes.center.x
                let normalizedDistance: CGFloat = distance / DimensionConstant.PromotionLayout.ACTIVE_ZOOM
                
                //test alpha animation func
                alphaAnimation(attributes)
                
                if abs(distance) < DimensionConstant.PromotionLayout.ACTIVE_ZOOM {
                    let zoom: CGFloat = 1 + DimensionConstant.PromotionLayout.ZOOM_FACTOR * (1 - abs(normalizedDistance))
                    attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0)
                    attributes.zIndex = Int(round(zoom))
                    if 1 - abs(normalizedDistance) >= 0.2 {
                        attributes.alpha = 1 - abs(normalizedDistance)
                    }
                }
            }
        }
        return array
    }
    
    //test alpha animation func
    func alphaAnimation(_ attributes: UICollectionViewLayoutAttributes) {
        //set position of cell from middle to upper
        if DeviceType.IS_IPHONE_4_OR_LESS {
            attributes.frame.origin.y = ((self.collectionView?.frame.width)! - itemWidth - itemSpacing*2)
        } else if DeviceType.IS_IPHONE_5 { //320
            attributes.frame.origin.y = ((self.collectionView?.frame.width)! - itemWidth)
        } else if DeviceType.IS_IPHONE_6 { //375
            attributes.frame.origin.y = ((self.collectionView?.frame.width)! - itemWidth)
        } else if DeviceType.IS_IPHONE_6P { //414
            attributes.frame.origin.y = ((self.collectionView?.frame.width)! - itemWidth)
        }
        attributes.alpha = 0.2
    }
    
    //from WWDC video
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    //size of collection view
    override var collectionViewContentSize : CGSize {
        //padding from collectionview to first item
        self.sectionInset = UIEdgeInsetsMake(0, ((self.collectionView?.frame.width)! - itemWidth)/2, 0, 0)
        let collectionViewHeight = self.collectionView!.frame.height
        let contentWidth: CGFloat = maxXPos + itemWidth + ((self.collectionView?.frame.width)! - itemWidth)
        return CGSize(width: contentWidth, height: collectionViewHeight)
    }
    
    //from WWDC video
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        var offsetAdjustment: CGFloat = DimensionConstant.PromotionLayout.MAX_FLOAT
        let horizontalCenter: CGFloat = proposedContentOffset.x + ((self.collectionView?.bounds)!.width / 2.0)
        let targetRect: CGRect = CGRect(x: proposedContentOffset.x, y: 0.0, width: (self.collectionView?.bounds.size.width)!, height: (self.collectionView?.bounds.size.height)!)
        let array: Array = super.layoutAttributesForElements(in: targetRect)!
        
        for layoutAttributes: UICollectionViewLayoutAttributes in array {
            
            let itemHorizontalCenter: CGFloat = layoutAttributes.center.x
            if abs(itemHorizontalCenter - horizontalCenter) < abs(offsetAdjustment) {
                offsetAdjustment = itemHorizontalCenter - horizontalCenter
            }
        }
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
    
    //center item when scrolling
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        if let cv = self.collectionView {
            let cvBounds = cv.bounds
            let halfWidth = cvBounds.size.width * 0.5
            let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth
            
            if let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) {
                var candidateAttributes : UICollectionViewLayoutAttributes?
                for attributes in attributesForVisibleCells {
                    //skip comparison with non-cell items (headers and footers)
                    if attributes.representedElementCategory != UICollectionElementCategory.cell {
                        continue
                    }
                    if let candAttrs = candidateAttributes {
                        let a = attributes.center.x - proposedContentOffsetCenterX
                        let b = candAttrs.center.x - proposedContentOffsetCenterX
                        
                        if fabsf(Float(a)) < fabsf(Float(b)) {
                            candidateAttributes = attributes
                        }
                    } else { //first time in the loop
                        candidateAttributes = attributes
                        continue
                    }
                }
                return CGPoint(x: round(candidateAttributes!.center.x - halfWidth), y: proposedContentOffset.y)
            }
        }
        //fallback
        return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
    }
}

