//
//  PromotionViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/29/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class PromotionViewModel: NSObject {
    var reloadTableViewCallback: (()->())!
    var dataArray: [PromotionResponse]?
    var paginationResponse: PaginationResponse?
    var delegate: PromotionViewController?
    
    init (reloadTableViewCallback : @escaping (() -> ())) {
        super.init()
        self.reloadTableViewCallback = reloadTableViewCallback
    }
    
    func reloadContent() {
        weak var weakSelf = self
        var page:Int = 0
        if let pagination = paginationResponse {
            if ((pagination.last) != nil && pagination.last == false) {
                page = pagination.number! + 1
            }
        }
        WMCLoyaltyPromotionRequest.init (callback: { (promotionResponse:[PromotionResponse]?, paginationResponse:PaginationResponse?, error:ErrorResponse?) -> Void in
            if let this = weakSelf {
                if let pR = promotionResponse {
                    //this.paginationResponse = page
                    this.dataArray = pR
                    this.reloadTableViewCallback()
                }
            }
            }, page:page).execute()
    }
    
    func switchSearch(_ lblPromotion:CGFloat, searchBar:CGFloat, btnSearch:CGFloat, btnUndo:CGFloat) {
        if let dele = delegate {
            dele.labelPromotion.alpha = lblPromotion
            dele.promotionSearchBar.alpha = searchBar
            dele.buttonSearch.alpha = btnSearch
            dele.buttonUndo.alpha = btnUndo
        }
    }
    
    func animateViewMoving(_ up:Bool) {
        if up {
            UIView.animate(withDuration: Double(0.3), animations: {
                self.switchSearch(0, searchBar: 1, btnSearch: 0, btnUndo: 1)
            })
        } else {
            UIView.animate(withDuration: Double(0.3), animations: {
                self.switchSearch(1, searchBar: 0, btnSearch: 1, btnUndo: 0)
            })
        }
    }
    
    func dismissKeyBoardWhileScrollInSearch() {
        if let dele = delegate {
            dele.isSearching = false
            dele.promotionSearchBar.resignFirstResponder()
        }
    }
    
    //check connection
    func checkConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(PromotionViewModel.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        checkConnectionStatus()
    }
    
    func checkConnectionStatus() {
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        case .online(.wwan):
            reloadContent()
        case .online(.wiFi):
            reloadContent()
        }
    }
    
    func networkStatusChanged(_ notification: Notification) {
        if Reach().connectionStatus().description == "Offline" || Reach().connectionStatus().description == "Unknown" {
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        }
    }
}
