//
//  PromotionDetailContainerViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/22/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class PromotionDetailContainerViewModel {
    var delegate: PromotionDetailContainerViewController?
    
    func showDetail() {
        if let dele = delegate {
            if let object = dele.passedObject {
                if let name = object.title {
                    if !name.isEmpty {
                        dele.labelName.text = name
                    }
                }
            }
        }
    }
    
    func countImage() -> Int {
        var count = 0
        if let dele = delegate {
            if let object = dele.passedObject {
                if let images = object.images {
                    if !images.isEmpty {
                        for i in 0 ..< images.count {
                            if let src = images[i].src {
                                if !src.isEmpty {
                                    count += 1
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if count < 6 {
            return count
        } else {
            return 5
        }
    }
}
