//
//  PromotionDetailViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/2/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class PromotionDetailViewModel {
    var delegate: PromotionDetailViewController?
    
    func showDetail() {
        if let dele = delegate {
            if let object = dele.passedObject {
                
                if let startDate = object.startDate,
                    let endDate = object.endDate {
                    dele.labelStartDate.text = Format.convertMiliToDate(startDate)
                    dele.labelEndDate.text = Format.convertMiliToDate(endDate)
                }
                
                if let status = object.status {
                    if !status.isEmpty {
                        dele.labelStatus.text = status
                    }
                }
                
                if let des = object.description {
                    if !des.isEmpty {
                        dele.labelDescription.text = des
                    }
                }
            }
        }
    }
    
//    func getLocation() -> String {
//        var str:String = ""
//        if let dele = delegate {
//            if let object = dele.passedObject {
//                if let outlet = object.outlets {
//                    if !outlet.isEmpty {
//                        for i in 0 ..< countLocation() {
//                            if i > 0 {
//                                str += "\n"
//                            }
//                            if let name = outlet[i].name {
//                                if !name.isEmpty {
//                                    str += name
//                                }
//                            }
//                            if let address = outlet[i].address,
//                                let city = address.city,
//                                let name = city.name {
//                                if !name.isEmpty {
//                                    str += " - " + city.name!
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return str
//    }
//    
    func countRules() -> Int {
        var count = 0
        if let dele = delegate {
            if let object = dele.passedObject {
                if let rule = object.rules {
                    if !rule.isEmpty {
                        for i in 0 ..< rule.count {
                            if let name = rule[i].title {
                                if !name.isEmpty {
                                    count += 1
                                }
                            }
                        }
                    }
                }
            }
        }
        return count
    }
}
