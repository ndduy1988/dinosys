//
//  MainMenuViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/6/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //constraint
    @IBOutlet weak var labelTitleTopHeight: NSLayoutConstraint!
    @IBOutlet weak var pageControlBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonSignInBottomHeight: NSLayoutConstraint!
    
    var viewModel: MainMenuViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
        initGCM()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    func initFirst() {
        viewModel = MainMenuViewModel(reloadCollectionViewCallback: reloadCollectionView)
        if let model = viewModel {
            model.delegate = self
            model.initCollectionView(collectionView)
            model.setConstraint()
            model.checkConnection()
        }
    }
    
    func reloadCollectionView() {
        if let cv = collectionView {
            cv.reloadData()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let model = viewModel {
            if let array = model.dataArray {
                if array.count < 6 {
                    pageControl.numberOfPages = array.count
                    return array.count
                } else {
                    pageControl.numberOfPages = 5
                    return 5
                }
            }
        }
        pageControl.numberOfPages = 0
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellConstant.CollectionCellIdentifier.promotion, for: indexPath) as! PromotionCollectionViewCell
        if let model = viewModel {
            if let array = model.dataArray {
                if let image = array[(indexPath as NSIndexPath).row].images {
                    if !image.isEmpty {
                        if let src = image[0].src {
                            if !src.isEmpty {
                                cell.imageView.contentMode = .scaleAspectFill
                                cell.imageView.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(src))!)
                            } else {
                                cell.imageView.contentMode = .scaleAspectFill
                                cell.imageView.image = UIImage(named: "empty_image")
                            }
                        } else {
                            cell.imageView.contentMode = .scaleAspectFill
                            cell.imageView.image = UIImage(named: "empty_image")
                        }
                    } else {
                        cell.imageView.contentMode = .scaleAspectFill
                        cell.imageView.image = UIImage(named: "empty_image")
                    }
                } else {
                    cell.imageView.contentMode = .scaleAspectFill
                    cell.imageView.image = UIImage(named: "empty_image")
                }
                
                if let title = array[(indexPath as NSIndexPath).row].title {
                    if !title.isEmpty {
                        cell.labelTitle.text = title
                    }
                }
                
                if let end = array[(indexPath as NSIndexPath).row].endDate {
                    cell.labelEndDate.text = "End " + Format.convertMiliToDate(end)
                }
                
                if let percentage = array[(indexPath as NSIndexPath).row].rules {
                    if getPercentage(percentage).ruleType == "" || getPercentage(percentage).highest == 0 {
                        cell.labelPercentage.text = ""
                    } else {
                        cell.labelPercentage.text = String("\(getPercentage(percentage).ruleType) \(getPercentage(percentage).highest)%")
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let model = viewModel {
            if let array = model.dataArray {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.PromotionDetailContainerViewController) as? PromotionDetailContainerViewController,
                    let nav = navigationController {
                    vc.passedObject = array[(indexPath as NSIndexPath).row]
                    nav.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let model = viewModel {
            model.fiveDots(collectionView, pageControl: pageControl)
        }
    }
    
    func getPercentage(_ rules: [RuleResponse]) -> (ruleType: String, highest: Int) {
        var highest = 0
        var ruleType = ""
        for i in 0..<rules.count {
            if let percentage = rules[i].percentage,
                let type = rules[i].ruleType {
                if percentage > highest {
                    highest = percentage
                    ruleType = type
                }
            }
        }
        if ruleType.contains("spend boost") {
            ruleType = "Spend Boost"
        }
        return (ruleType, highest)
    }
    
    // GCM
    func initGCM() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewController.updateRegistrationStatus(_:)),
                                                         name: NSNotification.Name(rawValue: appDelegate.registrationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewController.showReceivedMessage(_:)),
                                                         name: NSNotification.Name(rawValue: appDelegate.messageKey), object: nil)
    }
    
    func updateRegistrationStatus(_ notification: Notification) {
        if let info = (notification as NSNotification).userInfo as? Dictionary<String,String> {
            if let error = info["error"] {
                showAlert("Error registering with GCM", message: error)
            } else if let _ = info["registrationToken"] {
                //let message = "Check the xcode debug console for the registration token that you"
                //showAlert("Registration Successful!", message: message)
            }
        } else {
            print("Software failure. Guru meditation.")
        }
    }
    
    func showReceivedMessage(_ notification: Notification) {
        if let info = (notification as NSNotification).userInfo as? Dictionary<String,AnyObject> {
            if let aps = info["aps"] as? Dictionary<String, String> {
                showAlert("Message received", message: aps["alert"]!)
            }
        } else {
            print("Software failure. Guru meditation.")
        }
    }
    
    func showAlert(_ title:String, message:String) {
        let alert = UIAlertController(title: title,
                                      message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .destructive, handler: nil)
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

