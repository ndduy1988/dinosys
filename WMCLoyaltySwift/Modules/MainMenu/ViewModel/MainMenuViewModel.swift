//
//  MainMenuViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/28/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class MainMenuViewModel: NSObject {
    var reloadCollectionViewCallback: (()->())!
    var dataArray: [PromotionResponse]?
    var delegate: MainMenuViewController?
    
    init (reloadCollectionViewCallback : @escaping (() -> ())) {
        super.init()
        self.reloadCollectionViewCallback = reloadCollectionViewCallback
    }
    
    func reloadContent() {
        weak var weakSelf = self
        WMCLoyaltyPromotionRequest.init (callback: { (promotionResponse:[PromotionResponse]?, paginationResponse:PaginationResponse?, error:ErrorResponse?) -> Void in
            if let this = weakSelf {
                if let pR = promotionResponse {
                    this.dataArray = pR
                    this.reloadCollectionViewCallback()
                }
            }
            }, page:0).execute()
    }
    
    func initCollectionView(_ collectionView: UICollectionView) {
        //collectionView.backgroundColor = UIColor(patternImage: UIImage(named: "collection_background")!)
        collectionView.backgroundColor = UIColor(red: 24/255, green: 26/255, blue: 44/255, alpha: 1)
        collectionView.register(UINib(nibName: "PromotionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:CellConstant.CollectionCellIdentifier.promotion)
    }
    
    func fiveDots(_ collectionView: UICollectionView, pageControl: UIPageControl) {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            pageControl.currentPage = Int(collectionView.contentOffset.x / layout.itemSize.width)
        }
    }
    
    //SET CONSTRAINT
    func setConstraint() {
        if let dele = delegate {
            if DeviceType.IS_IPHONE_4_OR_LESS {
                dele.labelTitleTopHeight.constant = 5
                dele.pageControlBottomHeight.constant = 10
                dele.buttonSignInBottomHeight.constant = 10
            } else if DeviceType.IS_IPHONE_5 {
                dele.pageControlBottomHeight.constant = 17
                dele.buttonSignInBottomHeight.constant = 42
            } else if DeviceType.IS_IPHONE_6 {
                dele.pageControlBottomHeight.constant = 25
                dele.buttonSignInBottomHeight.constant = 58
            } else if DeviceType.IS_IPHONE_6P {
                dele.pageControlBottomHeight.constant = 30
                dele.buttonSignInBottomHeight.constant = 68
            }
        }
    }
    
    //check connection
    func checkConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(MainMenuViewModel.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        checkConnectionStatus()
    }
    
    func checkConnectionStatus() {
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        case .online(.wwan):
            reloadContent()
        case .online(.wiFi):
            reloadContent()
        }
    }
    
    func networkStatusChanged(_ notification: Notification) {
        if Reach().connectionStatus().description == "Offline" || Reach().connectionStatus().description == "Unknown" {
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        }
    }
}
