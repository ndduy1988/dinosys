//
//  UserSettingDetailViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/28/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class UserSettingDetailViewModel {
    var delegate: UserSettingDetailViewController?
    
    func signOut() {
        if let dele = delegate {
            Alert.signOut(dele, message: "Are you sure you want to sign out ?")
        }
    }
    
    func showDetail() {
        if let dele = delegate {
            if let object = dele.passedObject {
                
                if let memberSince = object.createDate {
                    dele.labelMemberSince.text = Format.convertMiliToDate(memberSince)
                }
                
                if let email = object.email {
                    if !email.isEmpty {
                        dele.labelEmail.text = email
                    }
                }
                
                if let password = object.password {
                    if !password.isEmpty {
                        dele.labelPassword.text = password
                    }
                }
                
                if let gender = object.gender {
                    if !gender.isEmpty {
                        dele.labelGender.text = gender
                    }
                }
                
                if let birthDay = object.birthday {
                    dele.labelBirthDay.text = Format.date(birthDay, format: "dd MMM yyyy")
                }
            }
        }
    }
}
