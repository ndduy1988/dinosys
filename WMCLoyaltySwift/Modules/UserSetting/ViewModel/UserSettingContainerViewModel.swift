//
//  UserSettingContainerViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/22/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

//
//  UserSettingDetailViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/28/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class UserSettingContainerViewModel {
    var delegate: UserSettingContainerViewController?
    
    func processBrowseImage(_ imagePicker: UIImagePickerController, dele: UIViewController) {
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController()
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Photo", style: .default) { action -> Void in
            imagePicker.sourceType = .camera
            dele.present(imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose Photo", style: .default) { action -> Void in
            imagePicker.sourceType = .photoLibrary
            dele.present(imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        dele.present(actionSheetController, animated: true, completion: nil)
    }
    
    func onEncoding(_ croppedImage:UIImage) {
        if let imageData = UIImageJPEGRepresentation(scaleImage(croppedImage, size:CGSize(width: DimensionConstant.Image.IMAGE_SIZE, height: DimensionConstant.Image.IMAGE_SIZE)), DimensionConstant.Image.COMPRESSION_QUALITY) {
            onUpload(imageData.base64EncodedString(options: .lineLength64Characters), croppedImage:croppedImage)
        }
    }
    
    func onUpload(_ base64String:String, croppedImage:UIImage) {
        if let dele = delegate {
            if let object = dele.passedObject {
                if let loyaltyNumber = object.loyaltyNumber {
                    if !base64String.isEmpty {
                        WMCLoyaltyProfileImageUploadRequest.init(callback: { (error:ErrorResponse?) -> Void in
                            if error == nil {
                                dele.imageView.image = croppedImage
                            }
                            }, loyaltyNumber:loyaltyNumber, imageBase64:base64String, imageExtension:"jpg").execute()
                    }
                }
            }
        }
    }
    
    func scaleImage(_ image:UIImage, size:CGSize) -> UIImage {
        if image.size.width <= size.width || image.size.height <= size.height {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(size, !false, 0.0)
        image.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    func showDetail() {
        if let dele = delegate {
            if let object = dele.passedObject {
                if let avatar = object.avatarUrl {
                    if !avatar.isEmpty {
                        dele.imageView.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(avatar))!)
                        dele.imageView.contentMode = .scaleAspectFill
                        customImage(dele.imageView)
                    }
                }
                
                if let prefix = object.prefix,
                    let firstName = object.firstName,
                    let lastName = object.lastName {
                    if !prefix.isEmpty && !firstName.isEmpty && !lastName.isEmpty {
                        dele.labelName.text = prefix + " " + firstName + " " + lastName
                    }
                }
            }
        }
    }
    
    func customImage(_ imageView: UIImageView) {
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor(red: 248/255, green: 234/255, blue: 165/255, alpha: 1).cgColor
        imageView.layer.cornerRadius = imageView.frame.size.width/2
        imageView.clipsToBounds = true
    }
}
