//
//  UserSettingContainerViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/22/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import RSKImageCropper

class UserSettingContainerViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate, RSKImageCropViewControllerDelegate {
    
    @IBOutlet weak var infoView: UIImageView!
    var passedObject: AccountResponse?
    var imagePicker: UIImagePickerController?
    var viewModel: UserSettingContainerViewModel?
    let transitionDelegate = TransitionDelegate()
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initFirst()
    }
    
    func initCorner() {
        infoView.layer.cornerRadius = 10
        infoView.clipsToBounds = true
    }
    
    func initFirst() {
        viewModel = UserSettingContainerViewModel()
        if let model = viewModel {
            model.delegate = self
            model.showDetail()
            initUploadImage()
        }
    }
    
    func initUploadImage() {
        imagePicker = UIImagePickerController()
        imagePicker!.delegate = self
        imagePicker!.allowsEditing = false
    }
    
    @IBAction func browseImagePressed(_ sender: AnyObject) {
        if let model = viewModel {
            if let iP = imagePicker {
                model.processBrowseImage(iP, dele: self)
                track.trackEvent(TrackingConstant.Screen.setting_screen, action: TrackingConstant.Action.change_image, label: "", value: 0)
            }
        }
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            var imageCropVC : RSKImageCropViewController!
            picker.dismiss(animated: true, completion: { () -> Void in
                imageCropVC = RSKImageCropViewController(image: pickedImage, cropMode: RSKImageCropMode.circle)
                imageCropVC.delegate = self
                self.present(imageCropVC, animated: true, completion: nil)
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        if let model = viewModel {
            model.onEncoding(croppedImage)
        }
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, willCropImage originalImage: UIImage) {
        dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.EmbedSegue.userSettingDetail {
            if let vc = segue.destination as? UserSettingDetailViewController {
                vc.passedObject = passedObject
            }
        } else if segue.identifier == SegueConstant.Segue.aboutContainer {
            if let vc = segue.destination as? AboutContainerViewController {
                vc.transitioningDelegate = transitionDelegate
            }
        }
    }
}
