//
//  UserSettingDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/30/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class UserSettingDetailViewController: UITableViewController {
    
    var passedObject: AccountResponse?
    var viewModel: UserSettingDetailViewModel?
    
    @IBOutlet weak var labelMemberSince: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var labelBirthDay: UILabel!
    
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.setting_screen)
    }
    
    func initFirst() {
        viewModel = UserSettingDetailViewModel()
        if let model = viewModel {
            model.delegate = self
            model.showDetail()
        }
    }
    
    @IBAction func onSignOutPressed(_ sender: AnyObject) {
        if let model = viewModel {
            model.signOut()
            track.trackEvent(TrackingConstant.Screen.setting_screen, action: TrackingConstant.Action.logout, label: "", value: 0)
        }
    }
}
