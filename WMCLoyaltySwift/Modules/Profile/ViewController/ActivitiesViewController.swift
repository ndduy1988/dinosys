//
//  ActivitiesPageViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/20/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var activitiesTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    var activityIndicatorView: NVActivityIndicatorView? = nil
    
    @IBOutlet weak var restaurantImageBorder: UIImageView!
    @IBOutlet weak var hotelImageBorder: UIImageView!
    
    @IBOutlet weak var paddingTopHeight: NSLayoutConstraint!
    
    var viewModel: ActivitiesViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initActivityIndicator()
        customPullToRequest()
        initFirst()
    }
    
    func customPullToRequest() {
        if let customSubview = Bundle.main.loadNibNamed("CustomSubview", owner: self, options: nil)?.first as? CustomSubview {
            activitiesTableView.addPullToRefreshWithAction({
                OperationQueue().addOperation {
                    sleep(1)
                    OperationQueue.main.addOperation {
                        self.initFirst()
                        self.activitiesTableView.stopPullToRefresh()
                    }
                }
                }, withAnimator: customSubview)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        addScreenTracking()
        activityIndicatorView!.startAnimating()
        reloadTableView()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.activities_list_screen)
    }
    
    func initFirst() {
        viewModel = ActivitiesViewModel(reloadTableViewCallback: reloadTableView)
        if let model = viewModel {
            model.delegate = self
            model.checkConnection()
            model.customImageView()
        }
    }
    
    func reloadTableView() {
        if let tv = activitiesTableView {
            tv.reloadData()
            if let indicator = activityIndicatorView {
                indicator.stopAnimating()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = viewModel {
            if let array = model.dataArray {
                emptyView.isHidden = showEmptyTableErrorMessage()
                return array.count
            }
        }
        emptyView.isHidden = showEmptyTableErrorMessage()
        return 0
    }
    
    func showEmptyTableErrorMessage() -> Bool {
        if let model = viewModel {
            if let array = model.dataArray {
                if array.count <= 0 {
                    return false
                }
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellConstant.TableCellIdentifier.activities, for: indexPath) as! ActivitiesTableViewCell
        
        cell.labelCircle.layer.cornerRadius = cell.labelCircle.frame.size.width/2
        cell.labelCircle.clipsToBounds = true
        
        if let model = viewModel {
            if let array = model.dataArray {
                
                if let date = array[(indexPath as NSIndexPath).row].receiptDate {
                    //cell.labelDate.text = Format.date(date, format:"dd MMM yyyy")
                    cell.labelDate.text = Format.convertMiliToDate(date)
                }
                
                if let outlet = array[(indexPath as NSIndexPath).row].outlet {
                    if let name = outlet.name {
                        if !name.isEmpty {
                            cell.labelName.text = name
                        }
                    }
                    
                    if let type = outlet.type {
                        if !type.isEmpty {
                            cell.labelType.text = type
                        }
                    }
                }
                
                if let total = array[(indexPath as NSIndexPath).row].total {
                    cell.labelTotalEarned.text = Format.currency(String(total), code:"VND")
                }
                if let netSale = array[(indexPath as NSIndexPath).row].netSale {
                    cell.labelQualifyingPoint.text = Format.currency(String(netSale), code:"VND")
                }
            }
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueConstant.Segue.activitiesDetailContainer {
            let vc = segue.destination as! ActivitiesDetailContainerViewController
            if let model = viewModel {
                if let array = model.dataArray,
                    let row = (activitiesTableView.indexPathForSelectedRow as NSIndexPath?)?.row {
                    vc.passedObject = array[row]
                }
            }
        }
    }
    
    @IBAction func onRestaurantPressed(_ sender: AnyObject) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.RestaurantViewController)
    }
    
    @IBAction func onHotelPressed(_ sender: AnyObject) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.HotelViewController)
    }
    
    func initActivityIndicator() {
        let activityTypes: [NVActivityIndicatorType] = [
            .ballPulse,
            .ballGridPulse,
            .ballClipRotate,
            .ballClipRotatePulse,
            .squareSpin,
            .ballClipRotateMultiple,
            .ballPulseRise,
            .ballRotate,
            .cubeTransition,
            .ballZigZag,
            .ballZigZagDeflect,
            .ballTrianglePath,
            .ballScale,
            .lineScale,
            .lineScaleParty,
            .ballScaleMultiple,
            .ballPulseSync,
            .ballBeat,
            .lineScalePulseOut,
            .lineScalePulseOutRapid,
            .ballScaleRipple,
            .ballScaleRippleMultiple,
            .ballSpinFadeLoader,
            .lineSpinFadeLoader,
            .triangleSkewSpin,
            .pacman,
            .ballGridBeat,
            .semiCircleSpin,
            .ballRotateChase]
        
        let frame = CGRect(x: self.view.frame.width/2 - (self.view.frame.width/12)/2, y: self.view.frame.height/3, width: self.view.frame.width/12, height: self.view.frame.width/12)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: activityTypes[13])
        self.view.addSubview(activityIndicatorView!)
    }
}
