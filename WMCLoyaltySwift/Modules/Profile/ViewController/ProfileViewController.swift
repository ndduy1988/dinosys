//
//  ProfileViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/6/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

@available(iOS 9.0, *)
class ProfileViewController: UIViewController, UIPageViewControllerDelegate {
    
    var ppvc: ProfilePageViewController?
    var viewModel: ProfileViewModel?
    
    @IBOutlet weak var slideBarLeading: NSLayoutConstraint!
    //IBOutlet
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var labelAccountBalance: UILabel!
    @IBOutlet weak var labelMemberShipLevel: UILabel!
    @IBOutlet weak var labelPaidNight: UILabel!
    
    //constant
    @IBOutlet weak var profileViewHeight: NSLayoutConstraint!
    @IBOutlet weak var profileImageViewHeight: NSLayoutConstraint!
    
    //collapse
    @IBOutlet weak var imageViewCollapse: UIImageView!
    @IBOutlet weak var labelNumberCollapse: UILabel!
    @IBOutlet weak var labelNameCollapse: UILabel!
    @IBOutlet weak var labelClubNumberCollapse: UILabel!
    
    //expand
    @IBOutlet weak var imageViewExpand: UIImageView!
    @IBOutlet weak var labelNameExpand: UILabel!
    @IBOutlet weak var labelCardNumberExpand: UILabel!
    @IBOutlet weak var labelNumberExpand: UILabel!
    
    //segment button
    @IBOutlet weak var buttonSummary: UIButton!
    @IBOutlet weak var buttonActivities: UIButton!
    
    @IBOutlet weak var imageCard: UIImageView!
    
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initFirst()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.profile_screen)
    }
    
    func initCorner() {
        imageCard.layer.cornerRadius = 10
        imageCard.clipsToBounds = true
        
        infoView.layer.cornerRadius = 10
        infoView.clipsToBounds = true
    }
    
    func slideBar(_ value:CGFloat) {
        self.slideBarLeading.constant = value
    }
    
    func initFirst() {
        viewModel = ProfileViewModel()
        if let model = viewModel {
            model.delegate = self
            model.switchView(0,lblNameCollapse: 0,lblClubNumberCollapse: 0,lblNumberCollapse: 0, ivExpand: 1,lblNameExpand: 1, lblCardNumberExpand: 1, lblNumberExpand: 1)
            model.switchColor(true)
        }
        ppvc?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
        if let model = viewModel {
            model.checkConnection()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueConstant.EmbedSegue.profilePage {
            if let vc = segue.destination as? ProfilePageViewController {
                ppvc = vc
            }
        } else if segue.identifier == SegueConstant.Segue.userSettingContainer {
            if let model = viewModel {
                if let profile = model.profile,
                    let vc = segue.destination as? UserSettingContainerViewController {
                    vc.passedObject = profile
                }
            }
        }
    }
    
    @IBAction func onSummaryPressed(_ sender: AnyObject) {
        track.trackEvent(TrackingConstant.Screen.profile_screen, action: TrackingConstant.Action.summary, label: "", value: 0)
        //
        if let index = ppvc?.segmentIndex {
            if index == 0 {
                return
            }
        }
        
        if let summaryPage = storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.SummaryViewController) {
            if ppvc != nil {
                ppvc!.setViewControllers([summaryPage], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
                ppvc?.segmentIndex = 0
                slideBar(0)
                if let model = viewModel {
                    model.switchColor(true)
                    model.animateWhenPressed(false)
                }
            }
        }
    }
    
    @IBAction func onActivitiesPressed(_ sender: AnyObject) {
        track.trackEvent(TrackingConstant.Screen.profile_screen, action: TrackingConstant.Action.activity, label: "", value: 0)
        //
        if let index = ppvc?.segmentIndex {
            if index == 1 {
                return
            }
        }
        
        if let activitiesPage = storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.ActivitiesViewController) {
            if ppvc != nil {
                ppvc!.setViewControllers([activitiesPage], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
                ppvc?.segmentIndex = 1
                slideBar(self.view.frame.size.width/2)
                if let model = viewModel {
                    model.switchColor(false)
                    model.animateWhenPressed(true)
                }
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let model = viewModel {
            model.scrollHorizontal(previousViewControllers, completed: completed, screenWidth: self.view.frame.size.width/2)
        }
    }
}
