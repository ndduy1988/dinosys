//
//  SummaryPageViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/20/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController {
    
    @IBOutlet weak var miniView: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var centerTextHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomTextHeight: NSLayoutConstraint!
    
    @IBOutlet weak var leftBottomText: NSLayoutConstraint!
    @IBOutlet weak var rightBottomText: NSLayoutConstraint!
    
    @IBOutlet weak var labelTotalQualifyingSpent: UILabel!
    @IBOutlet weak var labelCenter1: UILabel!
    @IBOutlet weak var labelCenter2: UILabel!
    @IBOutlet weak var labelCenter3: UILabel!
    @IBOutlet weak var labelPercent: UILabel!
    
    var viewModel: SummaryViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    func initFirst() {
        viewModel = SummaryViewModel()
        if let model = viewModel {
            model.delegate = self
            model.reloadContent()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.summary_screen)
    }
}
