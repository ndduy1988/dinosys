//
//  ActivitiesDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/23/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class ActivitiesDetailViewController: UITableViewController {
    
    var passedObject: TransactionResponse?
    var viewModel: ActivitiesDetailViewModel?
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTotalEarned: UILabel!
    @IBOutlet weak var labelQualifyingPoint: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    func initFirst() {
        viewModel = ActivitiesDetailViewModel()
        if let model = viewModel {
            model.delegate = self
            model.showDetail()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.activities_detail_screen)
    }
}

