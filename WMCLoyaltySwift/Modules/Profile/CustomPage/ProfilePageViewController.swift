//
//  ProfilePageViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/20/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class ProfilePageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pages = [UIViewController]()
    var segmentIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        initPage()
    }
    
    /////////////////////////////////// INIT & CUSTOM PAGES ///////////////////////////////////
    
    func initPage() {
        if let summaryPage: UIViewController? = storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.SummaryViewController),
            let activitiesPage: UIViewController? = storyboard?.instantiateViewController(withIdentifier: NotificationConstants.ViewController.ActivitiesViewController) {
                pages.append(summaryPage!)
                pages.append(activitiesPage!)
                setViewControllers([summaryPage!], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if pages.index(of: viewController) == nil {
            if segmentIndex == 1 {
                return pages[segmentIndex! - 1]
            }
        } else {
            let currentIndex = pages.index(of: viewController)!
            let previousIndex = abs((currentIndex - 1) % pages.count)
            if currentIndex == 1 {
                return pages[previousIndex]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if pages.index(of: viewController) == nil {
            if segmentIndex == 0 {
                return pages[segmentIndex! + 1]
            }
        } else {
            let currentIndex = pages.index(of: viewController)!
            let nextIndex = abs((currentIndex + 1) % pages.count)
            if currentIndex == 0 {
                return pages[nextIndex]
            }
        }
        return nil
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
