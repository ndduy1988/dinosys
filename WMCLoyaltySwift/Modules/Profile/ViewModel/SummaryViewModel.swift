//
//  SummaryViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/2/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import PNChart

class SummaryViewModel: NSObject, PNChartDelegate {
    
    var delegate: SummaryViewController?
    var profile: AccountResponse?
    
    var chartLeft: PNCircleChart?
    var chartCenter: PNCircleChart?
    var chartRight: PNCircleChart?
    
    func reloadContent() {
        weak var weakSelf = self
        WMCLoyaltyAccountRequest.init { (accountResponse:AccountResponse?, error:ErrorResponse?) -> Void in
            if let this = weakSelf {
                this.profile = accountResponse
                this.showDetails()
                this.customChart()
            }
            }.execute()
    }
    
    func customChart() {
        customHeight()
        customCenterChart()
        //customOtherChart()
    }
    
    func showDetails() {
        if let profile = profile,
            let dele = delegate {
            if let annualQualifyingSpent = profile.annualQualifyingSpent,
                let nextLevelSpent = profile.nextTierLevel?.qualifyingSpent {
                dele.labelCenter1.text = Format.currency(String(annualQualifyingSpent), code:"VND")
                dele.labelCenter2.text = "of " + Format.currency(String(nextLevelSpent), code:"VND")
                dele.labelPercent.text = String(format:"%.2f", (annualQualifyingSpent * 100) / nextLevelSpent) + " %"
                dele.labelTotalQualifyingSpent.text = Format.currency(String(annualQualifyingSpent), code:"VND")
            }
        }
    }
    
    func customHeight() {
        if let dele = delegate {
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                //custom scroll view height
                dele.viewHeight.constant = 235
                //text position
                dele.leftBottomText.constant = 22
                dele.rightBottomText.constant = 22
                dele.centerTextHeight.constant = 62
                dele.bottomTextHeight.constant = 46
                //text size
                dele.labelCenter1.font = UIFont(name:"MyriadPro-SemiExt", size: 18)
                dele.labelCenter2.font = UIFont(name:"MyriadPro-SemiExt", size: 11)
                dele.labelCenter3.font = UIFont(name:"MyriadPro-SemiExt", size: 11)
            } else if DeviceType.IS_IPHONE_6P {
                dele.leftBottomText.constant = 35
                dele.rightBottomText.constant = 35
                dele.centerTextHeight.constant = 85
                dele.bottomTextHeight.constant = 65
            }
        }
    }
    
    func customCenterChart() {
        if let dele = delegate {
            let CHART_CENTER = dele.view.frame.width/2.5
            var topToBottom = CHART_CENTER/2.4
            var lineWidth = 10
            
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                topToBottom = CHART_CENTER/2
                lineWidth = 10
            } else if DeviceType.IS_IPHONE_6 {
                lineWidth = 12
            } else if DeviceType.IS_IPHONE_6P {
                lineWidth = 14
            }
            
            if let annualQualifyingSpent = profile?.annualQualifyingSpent,
                let nextLevelSpent = profile?.nextTierLevel?.qualifyingSpent {
                
                let current = ((annualQualifyingSpent * 100) / nextLevelSpent)
                
                chartCenter = PNCircleChart(frame: CGRect(x: dele.view.frame.width/2 - CHART_CENTER/2, y: topToBottom, width: CHART_CENTER, height: CHART_CENTER), total: 100, current: current as NSNumber!, clockwise: false, shadow: true, shadowColor: ColorConstant.Summary.purpleColor, displayCountingLabel: false, overrideLineWidth: lineWidth as NSNumber!)
                
                if let center = chartCenter {
                    center.backgroundColor = UIColor.clear
                    center.strokeColor = ColorConstant.Summary.orangeColor
                    center.strokeColorGradientStart = ColorConstant.Summary.orangeColor
                    center.stroke()
                    dele.miniView.addSubview(center)
                }
            }
        }
    }
    
    func customOtherChart() {
        if let dele = delegate {
            let CHART_DIMENSION = dele.view.frame.width/3.75
            let PADDING = CHART_DIMENSION/15
            var topToBottom = CHART_DIMENSION/0.95
            var lineWidth = 10
            
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                topToBottom = CHART_DIMENSION/0.85
                lineWidth = 6
            } else if DeviceType.IS_IPHONE_6 {
                lineWidth = 8
            } else if DeviceType.IS_IPHONE_6P {
                lineWidth = 10
            }
            
            //chart left
            chartLeft = PNCircleChart(frame: CGRect(x: dele.view.frame.origin.x + PADDING, y: topToBottom, width: CHART_DIMENSION, height: CHART_DIMENSION), total: 100, current: 100, clockwise: false, shadow: true, shadowColor: ColorConstant.Summary.grayColor, displayCountingLabel: false, overrideLineWidth: lineWidth as NSNumber!)
            
            chartLeft!.backgroundColor = UIColor.clear
            chartLeft!.strokeColor = ColorConstant.Summary.purpleColor
            chartLeft!.strokeColorGradientStart = ColorConstant.Summary.purpleColor
            chartLeft!.stroke()
            dele.miniView.addSubview(chartLeft!)
            
            //chart right
            chartRight = PNCircleChart(frame: CGRect(x: dele.view.frame.width - CHART_DIMENSION - PADDING, y: topToBottom, width: CHART_DIMENSION, height: CHART_DIMENSION), total: 100, current: 0, clockwise: false, shadow: true, shadowColor: ColorConstant.Summary.grayColor, displayCountingLabel: false, overrideLineWidth: lineWidth as NSNumber!)
            
            chartRight!.backgroundColor = UIColor.clear
            chartRight!.strokeColor = ColorConstant.Summary.purpleColor
            chartRight!.strokeColorGradientStart = ColorConstant.Summary.purpleColor
            chartRight!.stroke()
            dele.miniView.addSubview(chartRight!)
        }
    }
}
