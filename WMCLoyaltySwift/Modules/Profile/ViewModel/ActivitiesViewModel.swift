//
//  ActivitiesViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/2/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class ActivitiesViewModel: NSObject {
    
    var reloadTableViewCallback: (()->())!
    var dataArray: [TransactionResponse]?
    var delegate: ActivitiesViewController?
    
    init (reloadTableViewCallback : @escaping (() -> ())) {
        super.init()
        self.reloadTableViewCallback = reloadTableViewCallback
    }
    
    func reloadContent() {
        weak var weakSelf = self
        WMCLoyaltyTransactionRequest.init { (transactionResponse:[TransactionResponse]?, error:ErrorResponse?) -> Void in
            if let this = weakSelf {
                this.dataArray = transactionResponse
                this.reloadTableViewCallback()
            }
            }.execute()
        
    }
    
    //check connection
    func checkConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(ActivitiesViewModel.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        checkConnectionStatus()
    }
    
    func checkConnectionStatus() {
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        case .online(.wwan):
            reloadContent()
        case .online(.wiFi):
            reloadContent()
        }
    }
    
    func networkStatusChanged(_ notification: Notification) {
        if Reach().connectionStatus().description == "Offline" || Reach().connectionStatus().description == "Unknown" {
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        }
    }
    
    ///////////////////////////////////CUSTOM IMAGE BORDER WHEN NO ACTIVITY///////////////////////////////////
    
    func customImageView() {
        if let dele = delegate {
            if DeviceType.IS_IPHONE_4_OR_LESS {
                dele.paddingTopHeight.constant = 25
            }
            customImageBorder(dele.restaurantImageBorder)
            customImageBorder(dele.hotelImageBorder)
        }
    }
    
    func customImageBorder(_ imageView:UIImageView) {
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor(red:89/255.0, green:89/255.0, blue:104/255.0, alpha: 1.0).cgColor
    }
}
