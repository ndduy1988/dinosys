//
//  ProfileViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/1/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

@available(iOS 9.0, *)
class ProfileViewModel: NSObject {
    
    var profile: AccountResponse?
    var delegate: ProfileViewController?
    
    func reloadContent() {
        weak var weakSelf = self
        WMCLoyaltyAccountRequest.init { (accountResponse:AccountResponse?, error:ErrorResponse?) -> Void in
            if let this = weakSelf {
                if let aR = accountResponse {
                    this.profile = aR
                    this.showDetails()
                }
            }
            }.execute()
    }
    
    func showDetails() {
        if let profile = profile,
            let dele = delegate {
            if let avatar = profile.avatarUrl {
                if !avatar.isEmpty {
                    dele.imageViewExpand.af_setImage(withURL: URL(string:NetworkConstant.APIPath.ProfileImage(avatar))!)
                    dele.imageViewExpand.contentMode = .scaleAspectFill
                    dele.imageViewCollapse.af_setImage(withURL: URL(string:NetworkConstant.APIPath.ProfileImage(avatar))!)
                    dele.imageViewCollapse.contentMode = .scaleAspectFill
                    
                    dele.imageViewExpand.layer.cornerRadius = dele.imageViewExpand.frame.size.width/2
                    dele.imageViewExpand.clipsToBounds = true
                    dele.imageViewCollapse.layer.cornerRadius = dele.imageViewCollapse.frame.size.width/2
                    dele.imageViewCollapse.clipsToBounds = true
                    
                    dele.imageViewExpand.layer.borderWidth = 2
                    dele.imageViewExpand.layer.borderColor = UIColor(red: 248/255, green: 234/255, blue: 165/255, alpha: 1).cgColor
                    
                    dele.imageViewCollapse.layer.borderWidth = 1
                    dele.imageViewCollapse.layer.borderColor = UIColor(red: 248/255, green: 234/255, blue: 165/255, alpha: 1).cgColor
                }
            }
            
            if let prefix = profile.prefix,
                let firstName = profile.firstName,
                let lastName = profile.lastName {
                if !prefix.isEmpty && !firstName.isEmpty && !lastName.isEmpty {
                    dele.labelNameCollapse.text = prefix + " " + firstName + " " + lastName
                    dele.labelNameExpand.text = prefix + " " + firstName + " " + lastName
                }
            }
            
            //blur view area
            if let accountBalance = profile.annualQualifyingSpent {
                dele.labelAccountBalance.text = Format.currency(String(accountBalance), code:"VND")
            }
            
            if let tierLevel = profile.tierLevel {
                if let levelName = tierLevel.levelName {
                    if !levelName.isEmpty {
                        dele.labelMemberShipLevel.text = levelName
                    }
                }
            }
            
            if let paidNights = profile.paidNight {
                dele.labelPaidNight.text = String(paidNights)
            }
            
            if let clubNumber = profile.loyaltyNumber {
                dele.labelNumberCollapse.text = " " + String(clubNumber)
                dele.labelNumberExpand.text = " " + String(clubNumber)
            }
        }
    }
    
    func scrollHorizontal(_ previousViewControllers: [UIViewController], completed: Bool, screenWidth: CGFloat) {
        if let dele = delegate,
            let ppvc = dele.ppvc {
            if previousViewControllers[0].isKind(of: SummaryViewController.self) && completed {
                switchColor(false)
                animateWhenScroll(true)
                dele.slideBar(screenWidth)
                ppvc.segmentIndex = 1
            }
            if previousViewControllers[0].isKind(of: ActivitiesViewController.self) && completed {
                switchColor(true)
                animateWhenScroll(false)
                dele.slideBar(0)
                ppvc.segmentIndex = 0
            }
        }
    }
    
    func switchColor(_ selected:Bool) {
        if let dele = delegate {
            dele.buttonSummary.isSelected = selected
            dele.buttonActivities.isSelected = !selected
        }
    }
    
    func animateWhenPressed(_ up:Bool) {
        if up {
            UIView.animate(withDuration: Double(0.7), animations: {
                self.switchView(1,lblNameCollapse: 1,lblClubNumberCollapse: 1,lblNumberCollapse: 1, ivExpand: 0,lblNameExpand: 0, lblCardNumberExpand: 0, lblNumberExpand: 0)
            })
            setConstraintImageView(145, duration: 0.4, delay: 0.4)
            setConstraintVisualView(90, duration: 0.4, delay: 0.4)
        } else {
            UIView.animate(withDuration: Double(0.9), animations: {
                self.switchView(0,lblNameCollapse: 0,lblClubNumberCollapse: 0,lblNumberCollapse: 0, ivExpand: 1,lblNameExpand: 1, lblCardNumberExpand: 1, lblNumberExpand: 1)
            })
            setConstraintImageView(210, duration: 0.4, delay: 0.3)
            setConstraintVisualView(235, duration: 0.4, delay: 0.3)
        }
    }
    
    func animateWhenScroll(_ up:Bool) {
        if up {
            UIView.animate(withDuration: Double(0), animations: {
                self.switchView(1,lblNameCollapse: 1,lblClubNumberCollapse: 1,lblNumberCollapse: 1, ivExpand: 0,lblNameExpand: 0, lblCardNumberExpand: 0, lblNumberExpand: 0)
            })
            setConstraintImageViewWithoutAnimation(145)
            setConstraintVisualViewWithoutAnimation(90)
        } else {
            UIView.animate(withDuration: Double(0.3), animations: {
                self.switchView(0,lblNameCollapse: 0,lblClubNumberCollapse: 0,lblNumberCollapse: 0, ivExpand: 1,lblNameExpand: 1, lblCardNumberExpand: 1, lblNumberExpand: 1)
            })
            setConstraintImageViewWithoutAnimation(210)
            setConstraintVisualViewWithoutAnimation(235)
        }
    }
    
    func setConstraintImageView(_ imageHeight:CGFloat, duration:TimeInterval, delay:TimeInterval) {
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: .allowUserInteraction, animations: {
            if let dele = self.delegate {
                dele.profileImageViewHeight.constant = imageHeight
                dele.view.layoutIfNeeded()
            }
            }, completion: nil)
    }
    
    func setConstraintVisualView(_ visualHeight:CGFloat, duration:TimeInterval, delay:TimeInterval) {
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: .allowUserInteraction, animations: {
            if let dele = self.delegate {
                dele.profileViewHeight.constant = visualHeight
                dele.view.layoutIfNeeded()
            }
            }, completion: nil)
    }
    
    func setConstraintImageViewWithoutAnimation(_ imageHeight:CGFloat) {
        if let dele = delegate {
            dele.profileImageViewHeight.constant = imageHeight
        }
    }
    
    func setConstraintVisualViewWithoutAnimation(_ visualHeight:CGFloat) {
        if let dele = delegate {
            dele.profileViewHeight.constant = visualHeight
        }
    }
    
    func switchView(_ ivCollapse:CGFloat, lblNameCollapse:CGFloat, lblClubNumberCollapse:CGFloat, lblNumberCollapse:CGFloat, ivExpand:CGFloat, lblNameExpand:CGFloat, lblCardNumberExpand:CGFloat, lblNumberExpand:CGFloat) {
        if let dele = delegate {
            dele.imageViewCollapse.alpha = ivCollapse
            dele.labelNameCollapse.alpha = lblNameCollapse
            dele.labelClubNumberCollapse.alpha = lblClubNumberCollapse
            dele.labelNumberCollapse.alpha = lblNumberCollapse
            
            dele.imageViewExpand.alpha = ivExpand
            dele.labelNameExpand.alpha = lblNameExpand
            
            dele.labelCardNumberExpand.alpha = lblCardNumberExpand
            dele.labelNumberExpand.alpha = lblNumberExpand
        }
    }
    
    func checkConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewModel.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        checkConnectionStatus()
    }
    
    func checkConnectionStatus() {
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        case .online(.wwan):
            reloadContent()
        case .online(.wiFi):
            reloadContent()
        }
    }
    
    func networkStatusChanged(_ notification: Notification) {
        if Reach().connectionStatus().description == "Offline" || Reach().connectionStatus().description == "Unknown" {
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        }
    }
}
