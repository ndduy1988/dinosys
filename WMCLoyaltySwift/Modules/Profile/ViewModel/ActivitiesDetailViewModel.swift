//
//  ActivitiesDetailViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/2/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class ActivitiesDetailViewModel {
    var delegate: ActivitiesDetailViewController?
    
    func showDetail() {
        if let dele = delegate {
            if let object = dele.passedObject {
                if let outlet = object.outlet {
                    if let type = outlet.type {
                        if !type.isEmpty {
                            dele.labelType.text = type
                        }
                    }
                    if let detail = outlet.description {
                        if !detail.isEmpty {
                            dele.labelDetails.text = detail
                        }
                    }
                    if let address = outlet.address {
                        if let city = address.city,
                            let country = address.country {
                            if let cityName = city.name,
                                let countryName = country.name {
                                if !cityName.isEmpty && !countryName.isEmpty {
                                    dele.labelLocation.text = cityName + ", " + countryName
                                }
                            }
                        }
                    }
                }
                
                if let date = object.receiptDate {
                    //dele.labelDate.text = Format.date(date, format: "dd MMM yyyy")
                    dele.labelDate.text = Format.convertMiliToDate(date)
                }
                
                if let total = object.total {
                    dele.labelTotalEarned.text = Format.currency(String(total), code:"VND")
                }
                
                if let netSale = object.netSale {
                    dele.labelQualifyingPoint.text = Format.currency(String(netSale), code:"VND")
                }
            }
        }
    }
}
