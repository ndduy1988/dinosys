//
//  ProfileTableViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/15/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class ActivitiesTableViewCell: UITableViewCell {
    @IBOutlet weak var labelCircle: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelTotalEarned: UILabel!
    @IBOutlet weak var labelQualifyingPoint: UILabel!
}
