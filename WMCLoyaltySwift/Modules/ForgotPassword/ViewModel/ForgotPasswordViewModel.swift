//
//  ForgotPasswordViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/27/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class ForgotPasswordViewModel {
    var delegate: ForgotPasswordViewController?
    
    func processSend() {
        if let dele = delegate {
            if let email = dele.textFieldForEmail,
                let error = dele.labelErrorMessage {
                    if checkTextField(textField: email, labelError: error, isCheckEmail:false) {
                        if Helper.isValidEmail(email.text!) {
                            onSend(email: email)
                            Helper.dismissKeyboard(email, passWord: email)
                        } else {
                            let check = checkTextField(textField: email, labelError: error, isCheckEmail:true)
                            print(check)
                        }
                    }
            }
        }
    }
    
    func onSend(email: UITextField) {
        if let email = email.text {
            WMCLoyaltyForgotPasswordRequest.init(callback: { (error:ErrorResponse?) -> Void in
                if let _ = error {
                    print("AAAAAAAAAAAA")
                } else {
                    print("BBBBBBBBBBBB")
                }
                }, email: email).execute()
        }
    }
    
    func checkTextField(textField: UITextField, labelError: UILabel, isCheckEmail:Bool) -> Bool {
        textField.layer.masksToBounds = true
        textField.layer.borderWidth = 1
        
        if isCheckEmail {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.shake(10, withDelta: 1.0, speed: 0.05)
            labelError.isHidden = false
            return false
        } else {
            if textField.text!.isEmpty {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.shake(10, withDelta: 1.0, speed: 0.05)
                labelError.isHidden = true
                return false
            }
        }
        labelError.isHidden = true
        textField.layer.borderColor = UIColor.gray.cgColor
        return true
    }
}
