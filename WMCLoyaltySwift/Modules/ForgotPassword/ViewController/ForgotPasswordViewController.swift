//
//  ForgotPasswordViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/16/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textFieldForEmail: UITextField!
    @IBOutlet weak var labelErrorMessage: UILabel!
    let viewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    func initFirst() {
        viewModel.delegate = self
        Helper.setTextFieldBorder(textFieldForEmail)
        textFieldForEmail.text = "luan.trinh@dinosys.vn"
    }
    
    @IBAction func dissmissKeyboardOnTap(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func onSendPressed(_ sender: AnyObject) {
        viewModel.processSend()
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return Helper.limitCharacter(textField, range: range, string: string)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewModel.processSend()
        return true
    }
}
