//
//  AboutViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/15/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var labelDescription: UILabel!
    var viewModel: AboutViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AboutViewModel()
        if let model = viewModel {
            model.delegate = self
            model.customScrollViewHeight()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.about_screen)
    }
}


