//
//  AboutViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/28/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class AboutViewModel {
    var delegate: AboutViewController?
    
    func customScrollViewHeight() {
        if let dele = delegate {
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                dele.viewHeight.constant = 910
            } else if DeviceType.IS_IPHONE_6 {
                dele.viewHeight.constant = 840
            } else if DeviceType.IS_IPHONE_6P {
                dele.viewHeight.constant = 820
            }
            dele.labelDescription.font = UIFont(name:"MyriadPro-SemiExt", size: 12)
        }
    }
}
