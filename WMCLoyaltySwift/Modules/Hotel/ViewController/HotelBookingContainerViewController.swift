//
//  HotelBookingContainerViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/23/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class HotelBookingContainerViewController: UIViewController {
    var passedObject: OutletResponse?
    @IBOutlet weak var infoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
    }
    
    func initCorner() {
        infoView.layer.cornerRadius = 4
        infoView.clipsToBounds = true
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.EmbedSegue.hotelBookingDetail {
            if let vc = segue.destination as? HotelBookingDetailViewController {
                vc.passedObject = passedObject
            }
        }
    }
}
