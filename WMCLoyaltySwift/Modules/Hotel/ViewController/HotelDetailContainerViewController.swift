//
//  HotelDetailContainerViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/22/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class HotelDetailContainerViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    var passedObject: OutletResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initFirst()
    }
    
    func initCorner() {
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
    }
    
    func initFirst() {
        if let object = passedObject {
            if let poster = object.posterUrl {
                if !poster.isEmpty {
                    imageView.contentMode = .scaleAspectFill
                    imageView.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(poster))!)
                    
                }
            }
            
            if let name = object.name {
                if !name.isEmpty {
                    labelName.text = name
                }
            }
        }
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.EmbedSegue.hotelDetail {
            if let vc = segue.destination as? HotelDetailViewController {
                vc.passedObject = passedObject
            }
        } else if segue.identifier == "hotelBookingSegue" {
            if let vc = segue.destination as? HotelBookingContainerViewController {
                vc.passedObject = passedObject
            }
        }
    }
}
