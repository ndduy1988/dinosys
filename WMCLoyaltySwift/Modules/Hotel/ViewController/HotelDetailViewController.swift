//
//  HotelDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/4/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class HotelDetailViewController: UITableViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    //cells
    @IBOutlet weak var cellInfo: UITableViewCell!
    @IBOutlet weak var cellMap: UITableViewCell!
    @IBOutlet weak var cellAddress: UITableViewCell!
    @IBOutlet weak var cellEmail: UITableViewCell!
    @IBOutlet weak var cellWebsite: UITableViewCell!
    @IBOutlet weak var cellRoomType: UITableViewCell!
    @IBOutlet weak var cellHotelPolicies: UITableViewCell!
    @IBOutlet weak var cellHotelDesctition: UITableViewCell!
    
    //IBOutlet
    @IBOutlet weak var routeMapView: MKMapView!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonPhone: UIButton!
    @IBOutlet weak var buttonFacebook: UIButton!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelWebsite: UILabel!
    @IBOutlet weak var labelHotelPolicies: UILabel!
    @IBOutlet weak var labelHotelDesctition: UILabel!
    
    var passedObject: OutletResponse?
    var viewModel: HotelDetailViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    var locationManager: CLLocationManager!
    
    var myRoute : MKRoute?
    var currentLat: Double?
    var currentLon: Double?
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var isMapCellExpand: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initFirstLoad()
    }
    
    func initCorner() {
        routeMapView.layer.cornerRadius = 4
        routeMapView.clipsToBounds = true
    }
    
    func initFirstLoad() {
        initLocationManager()
        viewModel = HotelDetailViewModel()
        if let model = viewModel {
            model.delegate = self
            model.loadingMap(1, mapView: 0)
            model.showDetail()
        }
        isMapCellExpand = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.hotel_detail_screen)
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    //////////////////////////////////SHOW CURRENT USER LOCATION//////////////////////////////////
    
    func initLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            if let map = routeMapView {
                map.tintColor = ColorConstant.Outlet.userLocation
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
            if (error != nil) {
                //print("ERROR: " + error!.localizedDescription)
                return
            }
            if let pm = placemarks?.first {
                weak var weakSelf = self
                if let this = weakSelf {
                    if let location = pm.location {
                        this.initRoute(location)
                        this.displayLocationInfo(pm)
                    }
                }
            }
        })
    }
    
    func displayLocationInfo(_ placemark: CLPlacemark) {
        if let lat = placemark.location?.coordinate.latitude,
            let lon = placemark.location?.coordinate.longitude {
            currentLat = lat
            currentLon = lon
        }
        
        //show current location
        if let subThoroughfare = placemark.subThoroughfare,
            let thoroughfare = placemark.thoroughfare,
            let subLocality = placemark.subLocality,
            let locality = placemark.locality,
            let country = placemark.country {
            if !subThoroughfare.isEmpty && !thoroughfare.isEmpty && !subLocality.isEmpty && !locality.isEmpty && !country.isEmpty {
                if let map = routeMapView {
                    map.userLocation.title = subThoroughfare + " " + thoroughfare + ", " + subLocality
                    map.userLocation.subtitle = locality + ", " + country
                }
            }
        }
    }
    
    /////////////////////////////////////////SHOW PINS ON MAP/////////////////////////////////////////
    
    func initRoute(_ location: CLLocation) {
        if let lat = currentLat,
            let lon = currentLon {
            if (lat == location.coordinate.latitude) && (lon == location.coordinate.longitude) {
                return
            }
        }
        
        let destination = MKPointAnnotation()
        
        if let destinationLat = passedObject?.lat,
            let destinationLon = passedObject?.lon,
            let title = passedObject?.name,
            let address = passedObject?.address,
            let fullAddress = address.fullAddress,
            let map = routeMapView {
            destination.coordinate = CLLocationCoordinate2DMake(destinationLat, destinationLon)
            destination.title = title
            destination.subtitle = fullAddress
            map.addAnnotation(destination)
        }
        
        let directionsRequest = MKDirectionsRequest()
        let markPoint1 = MKPlacemark(coordinate: CLLocationCoordinate2DMake(routeMapView.userLocation.coordinate.latitude, routeMapView.userLocation.coordinate.longitude), addressDictionary: nil)
        let markPoint2 = MKPlacemark(coordinate: CLLocationCoordinate2DMake(destination.coordinate.latitude, destination.coordinate.longitude), addressDictionary: nil)
        
        directionsRequest.source = MKMapItem(placemark: markPoint1)
        directionsRequest.destination = MKMapItem(placemark: markPoint2)
        
        directionsRequest.transportType = MKDirectionsTransportType.automobile
        let directions = MKDirections(request: directionsRequest)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.myRoute = unwrappedResponse.routes[0] as MKRoute
                self.routeMapView.add(route.polyline)
                if let model = self.viewModel {
                    model.loadingMapAnimation()
                    Helper.fitAnnotationOnMap(self.routeMapView, scale: 0.3)
                }
            }
        }
    }
    
    //Route
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            if let route = myRoute {
                let polylineRenderer = MKPolylineRenderer(overlay: (route.polyline))
                polylineRenderer.strokeColor = ColorConstant.Outlet.routeLine
                polylineRenderer.lineWidth = 2
                return polylineRenderer
            }
        }
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.image = UIImage(named:"pin_small")!
        } else {
            pinView!.annotation = annotation
        }
        return pinView
    }
    
    @IBAction func onCallNumberPressed(_ sender: AnyObject) {
        if let model = viewModel {
            model.onCallNumberPressed()
        }
    }
    
    @IBAction func onFaceBookPressed(_ sender: AnyObject) {
        if let model = viewModel {
            model.onFaceBookPressed()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).row == 0 {
            return cellInfo.frame.size.height
        } else if (indexPath as NSIndexPath).row == 1 {
            if isMapCellExpand == false {
                return 110
            } else {
                return routeMapView.frame.size.width
            }
        } else if (indexPath as NSIndexPath).row == 2 {
            if let address = passedObject?.address?.fullAddress {
                if !address.isEmpty {
                    return cellAddress.frame.size.height
                }
            } else {
                cellAddress.isHidden = true
            }
        } else if (indexPath as NSIndexPath).row == 3 {
            if let email = passedObject?.email {
                if !email.isEmpty {
                    return cellEmail.frame.size.height
                }
            } else {
                cellEmail.isHidden = true
            }
        } else if (indexPath as NSIndexPath).row == 4 {
            if let website = passedObject?.url {
                if !website.isEmpty {
                    return cellWebsite.frame.size.height
                }
            } else {
                cellWebsite.isHidden = true
            }
        } else if (indexPath as NSIndexPath).row == 5 {
            return  cellRoomType.frame.size.height
        } else if (indexPath as NSIndexPath).row == 6 {
            return  cellHotelPolicies.frame.size.height
        } else if (indexPath as NSIndexPath).row == 7 {
            return  cellHotelDesctition.frame.size.height
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = viewModel {
            model.didSelectRowAtIndexPath(tableView, indexPath: indexPath)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.Segue.hotelBookingContainer {
            if let vc = segue.destination as? HotelBookingContainerViewController {
                vc.passedObject = passedObject
            }
        }
    }
}
