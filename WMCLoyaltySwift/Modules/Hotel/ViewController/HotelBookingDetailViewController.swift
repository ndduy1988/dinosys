//
//  HotelBookingDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/23/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import SCLAlertView

class HotelBookingDetailViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var passedObject: OutletResponse?
    
    //cell
    @IBOutlet weak var cell1: UITableViewCell!
    @IBOutlet weak var cell2: UITableViewCell!
    @IBOutlet weak var cell3: UITableViewCell!
    @IBOutlet weak var cell4: UITableViewCell!
    @IBOutlet weak var cell5: UITableViewCell!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labelNumberOfRoom: UILabel!
    @IBOutlet weak var labelNumberOfPeople: UILabel!
    
    @IBOutlet weak var stepperNumberOfRoom: UIStepper!
    @IBOutlet weak var stepperNumberOfPeople: UIStepper!
    
    var viewModel: HotelBookingDetailViewModel?
    
    @IBOutlet weak var tfRoomType: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    
    var pickerData = ["Single Room", "Double Room", "Junior Suite", "Superior Suite", "Executive Suite", "Family Suite", "Grande Suite"]
    var imageData = ["single_room", "double_room", "junior_suite", "superior_suite", "executive_suite", "family_suite", "grande_suite"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initFirst()
    }
    
    func initCorner() {
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        
        cell1.layer.cornerRadius = 4
        cell1.clipsToBounds = true
        
        cell2.layer.cornerRadius = 4
        cell2.clipsToBounds = true
        
        cell3.layer.cornerRadius = 4
        cell3.clipsToBounds = true
        
        cell4.layer.cornerRadius = 4
        cell4.clipsToBounds = true
        
        cell5.layer.cornerRadius = 4
        cell5.clipsToBounds = true
    }
    
    func initFirst() {
        viewModel = HotelBookingDetailViewModel()
        if let model = viewModel {
            model.delegate = self
            model.initPicker()
            model.reloadContent()
        }
    }
    
    @IBAction func onConfirmPressed(_ sender: AnyObject) {
        let alertView = SCLAlertView()
        alertView.addButton("Book Now") {
            print("Book")
            self.bookingSuccess()
        }
        alertView.addButton("Cancel") {
            print("Cancel")
        }
        
        if compareDate() {
            if let object = passedObject {
                if let name = object.name {
                    if let noOfRoom = labelNumberOfRoom.text,
                        let noOfPeople = labelNumberOfPeople.text {
                        alertView.showWait(name, subTitle: "Number of People: " + noOfPeople + "\nNumber of Room: " + noOfRoom + "\nCheck-In Date: " + tfStartDate.text! + "\nCheck-Out Date: " + tfEndDate.text!)
                    }
                }
            }
        } else {
            wrongDate()
        }
    }
    
    func bookingSuccess() {
        let alertView = SCLAlertView()
        alertView.showSuccess("Successfully", subTitle: "You have successfully made the booking!", closeButtonTitle: nil, duration: 2)
    }
    
    func wrongDate() {
        let alertView = SCLAlertView()
        alertView.showError("Oops", subTitle: "There are something wrong with your Check-In Date and Check-Out Date", closeButtonTitle: "OK")
    }
    
    func compareDate() -> Bool {
        let now = dateTesting(tfStartDate.text!)
        let olderDate = dateTesting(tfEndDate.text!)
        
        let order = (Calendar.current as NSCalendar).compare(now, to: olderDate,
                                                             toUnitGranularity: .hour)
        
        switch order {
        case .orderedDescending:
            print("DESCENDING")
            return false
        case .orderedAscending:
            print("ASCENDING")
            return true
        case .orderedSame:
            print("SAME")
            return true
        }
    }
    
    @IBAction func onStepperNumberOfRoomPressed(_ sender: AnyObject) {
        labelNumberOfRoom.text = "\(Int(stepperNumberOfRoom.value))"
    }
    
    @IBAction func onStepperNumberOfPeoplePressed(_ sender: AnyObject) {
        labelNumberOfPeople.text = "\(Int(stepperNumberOfPeople.value))"
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        imageView.image = UIImage(named: imageData[row])
        tfRoomType.text = pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.text = pickerData[row]
        pickerLabel.font = UIFont(name: ".SFUIText-Light", size: 16)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
    
    func dateTesting(_ date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeZone = .current
        return formatter.date(from: date)!
    }
}
