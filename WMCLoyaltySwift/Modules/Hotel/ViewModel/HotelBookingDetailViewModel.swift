//
//  HotelBookingDetailViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 6/24/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import PKHUD

class HotelBookingDetailViewModel: NSObject {
    var isType: Bool?
    var isStartDate: Bool?
    var isEndDate: Bool?
    
    let datePickerView  : UIDatePicker = UIDatePicker()
    weak var actionToEnable : UIAlertAction?
    var delegate: HotelBookingDetailViewController?
    
    func reloadContent() {
        if let dele = delegate {
            dele.tfRoomType.text = dele.pickerData[0]
        }
    }
    
    func initPicker() {
        if let dele = delegate {
            //pickerView
            let pickerView = UIPickerView()
            pickerView.delegate = dele
            dele.tfRoomType.inputView = pickerView
            
            //datePicker
            datePickerView.datePickerMode = UIDatePickerMode.date
            datePickerView.minimumDate = Date()
            
            dele.tfStartDate.inputView = datePickerView
            dele.tfEndDate.inputView = datePickerView
            
            setDefaultDate()
            
            datePickerView.addTarget(self, action: #selector(HotelBookingDetailViewModel.handleDatePicker(_:)), for: UIControlEvents.valueChanged)
        }
    }
    
    func setDefaultDate() {
        if let dele = delegate {
            dele.tfStartDate.text = dateToString(Date())
            dele.tfEndDate.text = dateToString(Date())
        }
    }
    
    func handleDatePicker(_ sender: UIDatePicker) {
        if let dele = delegate {
            if dele.tfStartDate.isEditing == true {
                dele.tfStartDate.text = dateToString(sender.date)
            } else if dele.tfEndDate.isEditing == true {
                dele.tfEndDate.text = dateToString(sender.date)
            }
        }
    }
    
    func dateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter.string(from: date)
    }
    
    func stringToDate(_ date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        if let a = formatter.date(from: date) {
            return a
        }
        return Date()
    }
    
//    func setDateToDatePicker(num: Int) {
//        if let dele = delegate {
//            if let leaveObj = dele.passedObject {
//                if num == 1 {
//                    if let fromDate = leaveObj.fromDate {
//                        if !fromDate.isEmpty {
//                            if let d = dele.tfStartDate.text {
//                                if !d.isEmpty {
//                                    datePickerView.setDate(Helper.dateTesting(d), animated: true)
//                                }
//                            }
//                        }
//                    }
//                } else if num == 2 {
//                    if let toDate = leaveObj.toDate {
//                        if !toDate.isEmpty {
//                            if let d = dele.tfEndDate.text {
//                                if !d.isEmpty {
//                                    datePickerView.setDate(Helper.dateTesting(d), animated: true)
//                                }
//                            }
//                        }
//                    }
//                }
//            } else {
//                if num == 1 {
//                    if let d = dele.tfStartDate.text {
//                        if !d.isEmpty {
//                            datePickerView.setDate(stringToDate(d), animated: true)
//                        }
//                    }
//                } else if num == 2 {
//                    if let d = dele.tfEndDate.text {
//                        if !d.isEmpty {
//                            datePickerView.setDate(stringToDate(d), animated: true)
//                        }
//                    }
//                }
//            }
//        }
//    }
}
