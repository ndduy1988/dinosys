//
//  HotelTableViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 11/4/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class HotelTableViewCell: UITableViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var infoView: UIView!
}
