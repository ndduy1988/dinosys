//
//  TabBarViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/8/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController {
    //
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var buttonPromotion: UIButton!
    @IBOutlet weak var buttonRestaurant: UIButton!
    @IBOutlet weak var buttonHotel: UIButton!
    //
    @IBOutlet weak var buttonTextProfile: UIButton!
    @IBOutlet weak var buttonTextPromotion: UIButton!
    @IBOutlet weak var buttonTextRestaurant: UIButton!
    @IBOutlet weak var buttonTextHotel: UIButton!
    
    fileprivate var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(oldValue)
            updateActiveViewController()
        }
    }
    
    fileprivate func removeInactiveViewController(_ inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            inActiveVC.view.removeFromSuperview()
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    fileprivate func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            //iOS 9 still uses 49 points for the Tab Bar (and 64 points for a navigation bar)
            activeVC.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - DimensionConstant.TABBAR_HEIGHT)
            
            self.view.addSubview(activeVC.view)
            
            UIView.transition(with: self.view, duration: 0.3, options: .transitionCrossDissolve, animations: { _ in
                // call before adding child view controller's view as subview
                activeVC.didMove(toParentViewController: self)
                }, completion: nil)
        }
    }
    
    @objc func onRequestForNavigation(_ notification: Notification){
        let notificationInfo:String = notification.object as! String
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        switch notificationInfo {
        case NotificationConstants.ViewController.MainMenuViewController:
            let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.MainMenuViewController)
            switchColor(false, promotion: false, restaurant: false, hotel: false)
            activeViewController = vc
            break
        case NotificationConstants.ViewController.ProfileViewController:
            let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.ProfileViewController)
            switchColor(true, promotion: false, restaurant: false, hotel: false)
            activeViewController = vc
            break
        case NotificationConstants.ViewController.PromotionViewController:
            let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.PromotionViewController)
            switchColor(false, promotion: true, restaurant: false, hotel: false)
            activeViewController = vc
            break
        case NotificationConstants.ViewController.RestaurantViewController:
            let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.RestaurantViewController)
            switchColor(false, promotion: false, restaurant: true, hotel: false)
            activeViewController = vc
            break
        case NotificationConstants.ViewController.HotelViewController:
            let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.HotelViewController)
            switchColor(false, promotion: false, restaurant: false, hotel: true)
            activeViewController = vc
            break
        default:
            break
        }
    }
    
    /* Core class */
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(TabBarViewController.onRequestForNavigation(_:)),
            name: NSNotification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation),
            object: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.MainMenuViewController)
        
        if Store.sharedStore.user?.isLoggedIn() == true {
            onProfile()
        } else {
            activeViewController = vc
        }
    }
    
    @IBAction func onProfilePressed(_ sender: AnyObject) {
        checkLogin()
    }
    
    @IBAction func onPromotionPressed(_ sender: AnyObject) {
        onPromotion()
    }
    
    @IBAction func onRestaurantPressed(_ sender: AnyObject) {
        onRestaurant()
    }
    
    @IBAction func onHotelPressed(_ sender: AnyObject) {
        onHotel()
    }
    
    @IBAction func onButtonProfilePressed(_ sender: AnyObject) {
        checkLogin()
    }
    
    @IBAction func onButtonPromotionPressed(_ sender: AnyObject) {
        onPromotion()
    }
    
    @IBAction func onButtonRestaurantPressed(_ sender: AnyObject) {
        onRestaurant()
    }
    
    @IBAction func onButtonHotelPressed(_ sender: AnyObject) {
        onHotel()
    }
    
    func onProfile() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.ProfileViewController)
    }
    
    func onPromotion() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.PromotionViewController)
    }
    
    func onRestaurant() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.RestaurantViewController)
    }
    
    func onHotel() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.HotelViewController)
    }
    
    func checkLogin() {
        if Store.sharedStore.user?.isLoggedIn() == true {
            onProfile()
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.MainMenuViewController)
        }
    }
    
    func switchColor(_ profile:Bool, promotion:Bool, restaurant:Bool, hotel:Bool) {
        buttonProfile.isSelected = profile
        buttonPromotion.isSelected = promotion
        buttonRestaurant.isSelected = restaurant
        buttonHotel.isSelected = hotel
        
        buttonTextProfile.isSelected = buttonProfile.isSelected
        buttonTextPromotion.isSelected = buttonPromotion.isSelected
        buttonTextRestaurant.isSelected = buttonRestaurant.isSelected
        buttonTextHotel.isSelected = buttonHotel.isSelected
    }
}
