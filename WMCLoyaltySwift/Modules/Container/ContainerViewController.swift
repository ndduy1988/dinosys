//
//  ContainerViewController.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 6/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    /* Container view helper classes */
    fileprivate var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(oldValue)
            updateActiveViewController()
        }
    }
    
    fileprivate func removeInactiveViewController(_ inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMove(toParentViewController: nil)
            inActiveVC.view.removeFromSuperview()
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    fileprivate func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = self.view.bounds
            self.view.addSubview(activeVC.view)
            
            UIView.transition(with: self.view, duration: 0.3, options: .transitionCrossDissolve, animations: { _ in
                // call before adding child view controller's view as subview
                activeVC.didMove(toParentViewController: self)
                }, completion: nil)
        }
    }
    
    @objc func onRequestForNavigation(_ notification: Notification){
        let notificationInfo:String = notification.object as! String
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        switch notificationInfo {
        case NotificationConstants.ViewController.MainMenuNavigationController:
            let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.MainMenuNavigationController)
            activeViewController = vc
            break
        default:
            break
        }
    }
    
    /* Core class */
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ContainerViewController.onRequestForNavigation(_:)),
            name: NSNotification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation),
            object: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: NotificationConstants.ViewController.MainMenuNavigationController)
        
        //Setup VC
        activeViewController = vc
    }
}
