//
//  RestaurantViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/29/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RestaurantViewModel: NSObject {
    var reloadTableViewCallback: (()->())!
    var dataArray: [OutletResponse]?
    var paginationResponse: PaginationResponse?
    var delegate: RestaurantViewController?
    
    init (reloadTableViewCallback : @escaping (() -> ())) {
        super.init()
        self.reloadTableViewCallback = reloadTableViewCallback
    }
    
    func reloadContent() {
        weak var weakSelf = self
        var page:Int = 0
        if let pagination = paginationResponse {
            if ((pagination.last) != nil && pagination.last == false) {
                page = pagination.number! + 1
            }
        }
        WMCLoyaltyOutletByTypeRequest.init (callback: { (outletResponse:[OutletResponse]?, paginationResponse:PaginationResponse?, error:ErrorResponse?) -> Void in
            if let this = weakSelf {
                if let oR = outletResponse,
                    let pR = paginationResponse {
                        this.paginationResponse = pR
                        this.dataArray = oR
                        this.reloadTableViewCallback()
                }
            }
            }, page:page, type:EnumConstant.Outlet.OutletType.restaurant).execute()
    }
    
    func animateViewMoving(_ up:Bool) {
        if up {
            UIView.animate(withDuration: Double(0.3), animations: {
                self.switchSearch(0, searchBar: 1, btnBackOnMap: 0, btnBackToMap: 1, btnSearch: 0)
            })
            setConstraintMapView(0)
        } else {
            UIView.animate(withDuration: Double(0.3), animations: {
                self.switchSearch(1, searchBar: 0, btnBackOnMap: 0, btnBackToMap: 0, btnSearch: 1)
            })
            setConstraintMapView(DimensionConstant.Outlet.MAP_HEIGHT)
        }
    }
    
    func setConstraintMapView(_ mapHeight:CGFloat) {
        UIView.animate(withDuration: Double(0.5), animations: {
            if let dele = self.delegate {
                dele.myMapViewHeight.constant = mapHeight
                dele.view.layoutIfNeeded()
            }
        })
    }
    
    func switchSearch(_ lblRestaurant:CGFloat, searchBar:CGFloat, btnBackOnMap:CGFloat, btnBackToMap:CGFloat, btnSearch:CGFloat) {
        if let dele = self.delegate {
            dele.labelRestaurant.alpha = lblRestaurant
            dele.restaurantSearchBar.alpha = searchBar
            dele.buttonBackOnMap.alpha = btnBackOnMap
            dele.buttonBackToMap.alpha = btnBackToMap
            dele.buttonSearch.alpha = btnSearch
        }
    }
    
    //check connection
    func checkConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(RestaurantViewModel.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        checkConnectionStatus()
    }
    
    func checkConnectionStatus() {
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        case .online(.wwan):
            reloadContent()
        case .online(.wiFi):
            reloadContent()
        }
    }
    
    func networkStatusChanged(_ notification: Notification) {
        if Reach().connectionStatus().description == "Offline" || Reach().connectionStatus().description == "Unknown" {
            if let dele = delegate {
                Alert.checkConnection(dele)
            }
        }
    }
}
