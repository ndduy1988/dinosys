//
//  RestaurantDetailViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/2/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RestaurantDetailViewModel {
    var delegate: RestaurantDetailViewController?
    
    func didSelectRowAtIndexPath(_ tableView: UITableView, indexPath: IndexPath) {
        if let dele = delegate {
            if (indexPath as NSIndexPath).row == 1 {
                if dele.isMapCellExpand == false {
                    dele.isMapCellExpand = true
                } else {
                    dele.isMapCellExpand = false
                }
            } else if (indexPath as NSIndexPath).row == 5 {
                if dele.isDetailCellExpand == false {
                    dele.isDetailCellExpand = true
                } else {
                    dele.isDetailCellExpand = false
                }
            } else if (indexPath as NSIndexPath).row == 3 {
                if let dele = delegate {
                    if let object = dele.passedOutletObject {
                        if let email = object.email {
                            if !email.isEmpty {
                                UIApplication.shared.openURL(URL(string: "mailto:\(email)")!)
                            }
                        }
                    }
                }
            } else if (indexPath as NSIndexPath).row == 4 {
                if let dele = delegate {
                    if let object = dele.passedOutletObject {
                        if let website = object.url {
                            if !website.isEmpty {
                                if (website as NSString).contains("http") {
                                    UIApplication.shared.openURL(URL(string:website)!)
                                } else {
                                    UIApplication.shared.openURL(URL(string:"http://" + website)!)
                                }
                            }
                        }
                    }
                }
            }
            
            tableView.beginUpdates()
            tableView.endUpdates()
            Helper.fitAnnotationOnMap(dele.routeMapView, scale: 0.2)
        }
    }
    
    func onCallNumberPressed() {
        if let dele = delegate {
            if let object = dele.passedOutletObject {
                if let phone = object.telephone {
                    if !phone.isEmpty {
                        UIApplication.shared.openURL(URL(string:"telprompt:\(phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: ""))")!)
                    }
                }
            }
        }
    }
    
    func onFaceBookPressed() {
        if let dele = delegate {
            if let object = dele.passedOutletObject {
                if let facebook = object.facebook {
                    if !facebook.isEmpty {
                        if (facebook as NSString).contains("http") {
                            UIApplication.shared.openURL(URL(string:facebook)!)
                        } else {
                            UIApplication.shared.openURL(URL(string:"http://" + facebook)!)
                        }
                    }
                }
            }
        }
    }
    
    func showDetail() {
        if let dele = delegate {
            if let object = dele.passedOutletObject {
                if let time = object.opening {
                    if !time.isEmpty {
                        dele.labelTime.text = time
                    }
                }
                if let phone = object.telephone {
                    if !phone.isEmpty {
                        dele.buttonPhone.setTitle(phone, for: UIControlState())
                    }
                }
                if let facebook = object.facebook {
                    if !facebook.isEmpty {
                        dele.buttonFacebook.alpha = 1
                    }
                }
                if let address = object.address {
                    if let fullAddress = address.fullAddress {
                        if !fullAddress.isEmpty {
                            dele.labelAddress.text = fullAddress
                        }
                    }
                }
                if let email = object.email {
                    if !email.isEmpty {
                        dele.labelEmail.text = email
                    }
                }
                if let website = object.url {
                    if !website.isEmpty {
                        dele.labelWebsite.text = website
                    }
                }
                if let detail = object.description {
                    if !detail.isEmpty {
                        dele.labelDetails.text = detail
                    }
                }
            }
        }
    }
    
    func loadingMap(_ actInd:CGFloat, mapView:CGFloat) {
        if let dele = delegate {
            dele.activityIndicatorView.alpha = actInd
            dele.routeMapView.alpha = mapView
        }
    }
    
    func loadingMapAnimation() {
        UIView.animate(withDuration: Double(0.5), animations: {
            self.loadingMap(0, mapView: 1)
        })
    }
}
