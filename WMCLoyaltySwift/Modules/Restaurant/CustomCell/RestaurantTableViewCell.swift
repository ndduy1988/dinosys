//
//  FoodAndHotelTableViewCell.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/12/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var infoView: UIView!
}
