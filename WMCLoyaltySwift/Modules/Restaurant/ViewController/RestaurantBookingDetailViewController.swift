//
//  RestaurantBookingDetailViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/18/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import SCLAlertView
import CVCalendar

class RestaurantBookingDetailViewController: UITableViewController {
    
    //
    @IBOutlet weak var cell1: UITableViewCell!
    @IBOutlet weak var cell2: UITableViewCell!
    @IBOutlet weak var cell3: UITableViewCell!
    @IBOutlet weak var cell4: UITableViewCell!
    @IBOutlet weak var cell5: UITableViewCell!
    //
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!
    
    @IBOutlet weak var monthLabel: UILabel!
    
    var passedObject: OutletResponse?
    
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var numberOfPeople: UILabel!
    
    var shouldShowDaysOut = true
    var animationFinished = true
    
    var selectedDay:DayView!
    
    //hours
    @IBOutlet weak var hour0: UIButton!
    @IBOutlet weak var hour1: UIButton!
    @IBOutlet weak var hour2: UIButton!
    @IBOutlet weak var hour3: UIButton!
    @IBOutlet weak var hour4: UIButton!
    @IBOutlet weak var hour5: UIButton!
    @IBOutlet weak var hour6: UIButton!
    @IBOutlet weak var hour7: UIButton!
    @IBOutlet weak var hour8: UIButton!
    @IBOutlet weak var hour9: UIButton!
    @IBOutlet weak var hour10: UIButton!
    @IBOutlet weak var hour11: UIButton!
    @IBOutlet weak var hour12: UIButton!
    @IBOutlet weak var hour13: UIButton!
    @IBOutlet weak var hour14: UIButton!
    @IBOutlet weak var hour15: UIButton!
    @IBOutlet weak var hour16: UIButton!
    @IBOutlet weak var hour17: UIButton!
    @IBOutlet weak var hour18: UIButton!
    @IBOutlet weak var hour19: UIButton!
    @IBOutlet weak var hour20: UIButton!
    @IBOutlet weak var hour21: UIButton!
    @IBOutlet weak var hour22: UIButton!
    @IBOutlet weak var hour23: UIButton!
    
    //minutes
    @IBOutlet weak var min5: UIButton!
    @IBOutlet weak var min10: UIButton!
    @IBOutlet weak var min15: UIButton!
    @IBOutlet weak var min20: UIButton!
    @IBOutlet weak var min25: UIButton!
    @IBOutlet weak var min30: UIButton!
    @IBOutlet weak var min35: UIButton!
    @IBOutlet weak var min40: UIButton!
    @IBOutlet weak var min45: UIButton!
    @IBOutlet weak var min50: UIButton!
    @IBOutlet weak var min55: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        monthLabel.text = CVDate(date: Date()).globalDescription
    }
    
    func initCorner() {
        cell1.layer.cornerRadius = 4
        cell1.clipsToBounds = true
        
        cell2.layer.cornerRadius = 4
        cell2.clipsToBounds = true
        
        cell3.layer.cornerRadius = 4
        cell3.clipsToBounds = true
        
        cell4.layer.cornerRadius = 4
        cell4.clipsToBounds = true
        
        cell5.layer.cornerRadius = 4
        cell5.clipsToBounds = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        menuView.dayOfWeekTextColor = .white
        
        calendarView.appearance.dayLabelWeekdaySelectedTextColor = UIColor.black
        calendarView.appearance.dayLabelWeekdaySelectedBackgroundColor = UIColor.init(red: 248/255, green: 234/255, blue: 165/255, alpha: 1.0)
        
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
    }
    
    @IBAction func onConfirmPressed(_ sender: AnyObject) {
        let alertView = SCLAlertView()
        alertView.addButton("Book Now") {
            print("Book")
            self.bookingSuccess()
        }
        alertView.addButton("Cancel") {
            print("Cancel")
        }
        
        //        if let object = passedObject {
        //            if let name = object.name {
        //
        //                if let no = numberOfPeople.text {
        //                                        let s = "No of People: " + no + "\nTime: " + timeFormatter.stringFromDate(timePicker.date) + "\nDate: " + dateFormatter.stringFromDate(datePicker.date)
        //
        //                                        alertView.showWait(name, subTitle: s)
        //                }
        //            }
        //        }
    }
    
    func bookingSuccess() {
        let alertView = SCLAlertView()
        alertView.showSuccess("Successfully", subTitle: "You have successfully made the booking!", closeButtonTitle: nil, duration: 2)
    }
    
    @IBAction func onStepperPressed(_ sender: AnyObject) {
        numberOfPeople.text = "\(Int(stepper.value))"
    }
    
    /////////////////////////////////////////////////////////////////////////////////////HOURS
    
    @IBAction func onHourPressed(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            switchHourColor(self.hour0)
        case 1:
            switchHourColor(self.hour1)
        case 2:
            switchHourColor(self.hour2)
        case 3:
            switchHourColor(self.hour3)
        case 4:
            switchHourColor(self.hour4)
        case 5:
            switchHourColor(self.hour5)
        case 6:
            switchHourColor(self.hour6)
        case 7:
            switchHourColor(self.hour7)
        case 8:
            switchHourColor(self.hour8)
        case 9:
            switchHourColor(self.hour9)
        case 10:
            switchHourColor(self.hour10)
        case 11:
            switchHourColor(self.hour11)
        case 12:
            switchHourColor(self.hour12)
        case 13:
            switchHourColor(self.hour13)
        case 14:
            switchHourColor(self.hour14)
        case 15:
            switchHourColor(self.hour15)
        case 16:
            switchHourColor(self.hour16)
        case 17:
            switchHourColor(self.hour17)
        case 18:
            switchHourColor(self.hour18)
        case 19:
            switchHourColor(self.hour19)
        case 20:
            switchHourColor(self.hour20)
        case 21:
            switchHourColor(self.hour21)
        case 22:
            switchHourColor(self.hour22)
        case 23:
            switchHourColor(self.hour23)
        default : break
        }
    }
    
    func switchHourColor(_ button: UIButton) {
        self.hour0.isSelected = false
        self.hour1.isSelected = false
        self.hour2.isSelected = false
        self.hour3.isSelected = false
        self.hour4.isSelected = false
        self.hour5.isSelected = false
        self.hour6.isSelected = false
        self.hour7.isSelected = false
        self.hour8.isSelected = false
        self.hour9.isSelected = false
        self.hour10.isSelected = false
        self.hour11.isSelected = false
        self.hour12.isSelected = false
        self.hour13.isSelected = false
        self.hour14.isSelected = false
        self.hour15.isSelected = false
        self.hour16.isSelected = false
        self.hour17.isSelected = false
        self.hour18.isSelected = false
        self.hour19.isSelected = false
        self.hour20.isSelected = false
        self.hour21.isSelected = false
        self.hour22.isSelected = false
        self.hour23.isSelected = false
        
        button.isSelected = true
    }
    
    /////////////////////////////////////////////////////////////////////////////////////MINUTES
    
    @IBAction func onMinPressed(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            switchMinColor(self.min5)
        case 1:
            switchMinColor(self.min10)
        case 2:
            switchMinColor(self.min15)
        case 3:
            switchMinColor(self.min20)
        case 4:
            switchMinColor(self.min25)
        case 5:
            switchMinColor(self.min30)
        case 6:
            switchMinColor(self.min35)
        case 7:
            switchMinColor(self.min40)
        case 8:
            switchMinColor(self.min45)
        case 9:
            switchMinColor(self.min50)
        case 10:
            switchMinColor(self.min55)
        default : break
        }
    }
    
    func switchMinColor(_ button: UIButton) {
        self.min5.isSelected = false
        self.min10.isSelected = false
        self.min15.isSelected = false
        self.min20.isSelected = false
        self.min25.isSelected = false
        self.min30.isSelected = false
        self.min35.isSelected = false
        self.min40.isSelected = false
        self.min45.isSelected = false
        self.min50.isSelected = false
        self.min55.isSelected = false
        
        button.isSelected = true
    }
}

// MARK: - CVCalendarViewDelegate & CVCalendarMenuViewDelegate

extension RestaurantBookingDetailViewController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    /// Required method to implement!
    func presentationMode() -> CalendarMode {
        return .weekView
    }
    
    /// Required method to implement!
    func firstWeekday() -> Weekday {
        return .monday
    }
    
    func dayOfWeekTextColor() -> UIColor {
        return UIColor.white
    }
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        print("\(dayView.date.commonDescription) is selected!")
        selectedDay = dayView
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        if monthLabel.text != date.globalDescription && animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
            updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
                self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransform.identity
                
            }) { _ in
                
                self.animationFinished = true
                self.monthLabel.frame = updatedMonthLabel.frame
                self.monthLabel.text = updatedMonthLabel.text
                self.monthLabel.transform = CGAffineTransform.identity
                self.monthLabel.alpha = 1
                updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: monthLabel)
        }
    }
}

// MARK: - CVCalendarViewAppearanceDelegate
extension RestaurantBookingDetailViewController: CVCalendarViewAppearanceDelegate {
    func dayLabelPresentWeekdayInitallyBold() -> Bool {
        return true
    }
    
    func spaceBetweenDayViews() -> CGFloat {
        return 2
    }
    
    func dayLabelWeekdayInTextColor() -> UIColor {
        return UIColor.white
    }
    
    func dayLabelPresentWeekdayTextColor() -> UIColor {
        return UIColor(red: 223/255, green: 154/255, blue: 121/255, alpha: 1)
    }
    
    func dayLabelWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor(red: 248/255, green: 234/255, blue: 165/255, alpha: 1)
    }
}

