//
//  RestaurantBookingContainerViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 2/22/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

class RestaurantBookingContainerViewController: UIViewController {
    var passedObject: OutletResponse?
    
    @IBOutlet weak var restaurantInfo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
    }
    
    func initCorner() {
        restaurantInfo.layer.cornerRadius = 4
        restaurantInfo.clipsToBounds = true
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.EmbedSegue.restaurantBookingDetail {
            if let vc = segue.destination as? RestaurantBookingDetailViewController {
                vc.passedObject = passedObject
            }
        }
    }
}
