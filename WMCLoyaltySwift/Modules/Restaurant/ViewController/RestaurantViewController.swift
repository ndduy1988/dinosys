//
//  RestaurantViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/7/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import NVActivityIndicatorView

class RestaurantViewController: UIViewController, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var restaurantTableView: UITableView!
    @IBOutlet weak var myMapView: MKMapView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var spaceView: UIView!
    
    var activityIndicatorView: NVActivityIndicatorView? = nil
    @IBOutlet weak var lblNoData: UILabel!
    //constant
    @IBOutlet weak var myMapViewHeight: NSLayoutConstraint!
    
    //navi manual
    @IBOutlet weak var labelRestaurant: UILabel!
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var buttonBackOnMap: UIButton!
    @IBOutlet weak var restaurantSearchBar: UISearchBar!
    @IBOutlet weak var buttonBackToMap: UIButton!
    
    //search properties
    var filtered: [OutletResponse] = []
    var isSearching: Bool = false
    
    var currentLat: Double?
    var currentLon: Double?
    var distanceArray: [Double] = []
    
    //
    var selectedAnnotationTitle: String?
    var isSelectedAnnotation: Bool = false
    
    var locationManager: CLLocationManager!
    var viewModel: RestaurantViewModel?
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCorner()
        initActivityIndicator()
        customPullToRequest()
        initFirst()
    }
    
    func initCorner() {
        myMapView.layer.cornerRadius = 4
        myMapView.clipsToBounds = true
        
        infoView.layer.cornerRadius = 4
        infoView.clipsToBounds = true
    }
    
    func customPullToRequest() {
        if let customSubview = Bundle.main.loadNibNamed("CustomSubview", owner: self, options: nil)?.first as? CustomSubview {
            restaurantTableView.addPullToRefreshWithAction({
                OperationQueue().addOperation {
                    sleep(1)
                    OperationQueue.main.addOperation {
                        self.initFirst()
                        self.restaurantTableView.stopPullToRefresh()
                    }
                }
                }, withAnimator: customSubview)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
        activityIndicatorView!.startAnimating()
        reloadTableView()
        reloadSetting()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.restaurant_list_screen)
    }
    
    func trackEvent(_ value: Int) {
        //track.trackEvent(TrackingConstant.Screen.restaurant_list_screen, action: TrackingConstant.Action.restaurant_select, label: "", value: value)
    }
    
    func initFirst() {
        initLocationManager()
        viewModel = RestaurantViewModel(reloadTableViewCallback: reloadTableView)
        if let model = viewModel {
            model.delegate = self
            model.checkConnection()
        }
    }
    
    func reloadSetting() {
        if myMapViewHeight.constant != DimensionConstant.Outlet.MAP_HEIGHT {
            if let model = viewModel {
                model.animateViewMoving(false)
            }
        } else if let model = viewModel {
            model.switchSearch(1, searchBar: 0, btnBackOnMap: 0, btnBackToMap: 0, btnSearch: 1)
        }
        reloadSelectAnnotation()
        Helper.AnimateTable(restaurantTableView)
        restaurantSearchBar.text = ""
    }
    
    func reloadTableView() {
        if let tv = restaurantTableView {
            tv.reloadData()
            if let indicator = activityIndicatorView {
                indicator.stopAnimating()
            }
        }
    }
    
    //////////////////////////////////SHOW CURRENT USER LOCATION//////////////////////////////////
    
    func initLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            if let map = myMapView {
                map.tintColor = ColorConstant.Outlet.userLocation
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                return
            }
            if let pm = placemarks?.first {
                weak var weakSelf = self
                if let this = weakSelf {
                    if let location = pm.location {
                        this.initMap(location)
                        this.displayLocationInfo(pm)
                    }
                }
            }
        })
    }
    
    func displayLocationInfo(_ placemark: CLPlacemark) {
        if let lat = placemark.location?.coordinate.latitude,
            let lon = placemark.location?.coordinate.longitude {
            currentLat = lat
            currentLon = lon
        }
        
        //show current location
        if let subThoroughfare = placemark.subThoroughfare,
            let thoroughfare = placemark.thoroughfare,
            let subLocality = placemark.subLocality,
            let locality = placemark.locality,
            let country = placemark.country {
            if !subThoroughfare.isEmpty && !thoroughfare.isEmpty && !subLocality.isEmpty && !locality.isEmpty && !country.isEmpty {
                if let map = myMapView {
                    map.userLocation.title = subThoroughfare + " " + thoroughfare + ", " + subLocality
                    map.userLocation.subtitle = locality + ", " + country
                }
            }
        }
    }
    
    /////////////////////////////////////////SHOW PINS ON MAP/////////////////////////////////////////
    
    func initMap(_ location: CLLocation) {
        if let lat = currentLat,
            let lon = currentLon {
            if (lat == location.coordinate.latitude) && (lon == location.coordinate.longitude) {
                return
            }
        }
        
        let center: CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let mySpan: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let myRegion: MKCoordinateRegion = MKCoordinateRegionMake(center, mySpan)
        myMapView.setCenter(center, animated: true)
        myMapView.region = myRegion
        
        if let model = viewModel {
            if let array = model.dataArray {
                for i in 0 ..< array.count {
                    if let lon = array[i].lon,
                        let lat = array[i].lat,
                        let name = array[i].name,
                        let address = array[i].address,
                        let fullAddress = address.fullAddress,
                        let map = myMapView {
                        let annotation = MKPointAnnotation()
                        annotation.coordinate.latitude = lat
                        annotation.coordinate.longitude = lon
                        annotation.title = name
                        annotation.subtitle = fullAddress
                        mapView(map, viewFor: annotation)!.annotation = annotation
                        map.addAnnotation(annotation)
                    }
                }
            }
        }
        
        //calculate 1st load
        calculateDistance()
    }
    
    func calculateDistance() {
        distanceArray.removeAll()
        
        if isSearching {
            for i in 0 ..< filtered.count {
                if let lon = filtered[i].lon,
                    let lat = filtered[i].lat {
                    if let distance = locationManager.location?.distance(from: CLLocation(latitude: lat, longitude: lon)) {
                        distanceArray.append(Double(distance))
                    } else {
                        distanceArray.append(0.0)
                    }
                } else {
                    distanceArray.append(0.0)
                }
            }
        } else if isSelectedAnnotation {
            if let model = viewModel {
                if let array = model.dataArray {
                    for i in 0 ..< array.count {
                        if let name = array[i].name {
                            if !name.isEmpty {
                                if selectedAnnotationTitle == name {
                                    if let lat = array[i].lat,
                                        let lon = array[i].lon {
                                        if let distance = locationManager.location?.distance(from: CLLocation(latitude: lat, longitude: lon)) {
                                            distanceArray.append(Double(distance))
                                        } else {
                                            distanceArray.append(0.0)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if let model = viewModel {
                if let array = model.dataArray {
                    for i in 0 ..< array.count {
                        if let lon = array[i].lon,
                            let lat = array[i].lat {
                            if let distance = locationManager.location?.distance(from: CLLocation(latitude: lat, longitude: lon)) {
                                distanceArray.append(Double(distance))
                            } else {
                                distanceArray.append(0.0)
                            }
                        } else {
                            distanceArray.append(0.0)
                        }
                    }
                }
            }
        }
        reloadTableView()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.image = UIImage(named:"pin_big")!
            
            //            // Add logo to left callout
            //            let logo = UIImageView(image: UIImage(named: "pin_big"))
            //            pinView!.leftCalloutAccessoryView = logo
            //
            //            // Add detail button to right callout
            //            let detail = UIButton(type: UIButtonType.DetailDisclosure)
            //            pinView!.rightCalloutAccessoryView = detail
            
        } else {
            pinView!.annotation = annotation
        }
        return pinView
    }
    
    //    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    //        if control == view.rightCalloutAccessoryView {
    //            if let title = view.annotation?.title {
    //                selectedAnnotationTitle = title
    //                isSelectedAnnotation = true
    //                self.performSegueWithIdentifier(SegueConstant.Segue.restaurantDetailContainer, sender: nil)
    //            }
    //        }
    //    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let userLat = myMapView.userLocation.location?.coordinate.latitude,
            let userLon = myMapView.userLocation.location?.coordinate.longitude,
            let lat = view.annotation?.coordinate.latitude,
            let lon = view.annotation?.coordinate.longitude,
            let title = view.annotation?.title {
            if lat != userLat && lon != userLon {
                if myMapViewHeight.constant > DimensionConstant.Outlet.MAP_HEIGHT {
                    selectedAnnotationTitle = title
                    isSelectedAnnotation = true
                    calculateDistance()
                }
            }
        }
    }
    
    func reloadSelectAnnotation() {
        isSelectedAnnotation = false
        reloadTableView()
    }
    
    @IBAction func onMapPressed(_ sender: AnyObject) {
        buttonBackOnMap.alpha = 1
        buttonSearch.alpha = 0
        if let model = viewModel {
            model.setConstraintMapView(self.view.frame.height - DimensionConstant.TABBAR_HEIGHT - DimensionConstant.Outlet.RESTAURANT_CELL_HEIGHT)
        }
    }
    
    @IBAction func onBackOnMapPressed(_ sender: AnyObject) {
        buttonBackOnMap.alpha = 0
        buttonSearch.alpha = 1
        if let model = viewModel {
            model.setConstraintMapView(DimensionConstant.Outlet.MAP_HEIGHT)
        }
        reloadSelectAnnotation()
        calculateDistance()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filtered.count
        } else if isSelectedAnnotation {
            return 1
        } else if let model = viewModel {
            if let array = model.dataArray {
                lblNoData.isHidden = showEmptyTableErrorMessage()
                return array.count
            }
        }
        lblNoData.isHidden = showEmptyTableErrorMessage()
        return 0
    }
    
    func showEmptyTableErrorMessage() -> Bool {
        if let model = viewModel {
            if let array = model.dataArray {
                if array.count <= 0 {
                    return false
                }
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellConstant.TableCellIdentifier.restaurant, for: indexPath) as! RestaurantTableViewCell
        
        cell.infoView.layer.cornerRadius = 4
        cell.infoView.clipsToBounds = true
        
        if isSearching {
            if let logo = filtered[(indexPath as NSIndexPath).row].logoUrl {
                if !logo.isEmpty {
                    cell.imageCell.contentMode = .scaleAspectFit
                    cell.imageCell.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(logo))!)
                } else {
                    cell.imageCell.contentMode = .scaleAspectFill
                    cell.imageCell.image = UIImage(named: "empty_image")
                }
            } else {
                cell.imageCell.contentMode = .scaleAspectFill
                cell.imageCell.image = UIImage(named: "empty_image")
            }
            
            if let name = filtered[(indexPath as NSIndexPath).row].name {
                if !name.isEmpty {
                    cell.labelName.text = name
                }
            }
            
            if let address = filtered[(indexPath as NSIndexPath).row].address {
                if let fullAddress = address.fullAddress {
                    if !fullAddress.isEmpty {
                        cell.labelAddress.text = fullAddress
                    }
                }
            }
            
            if self.distanceArray.count == filtered.count {
                cell.labelDistance.text = String(format:"%.2f", self.distanceArray[(indexPath as NSIndexPath).row]/1000) + " km"
            }
        } else if isSelectedAnnotation {
            if let model = viewModel {
                if let array = model.dataArray {
                    for i in 0 ..< array.count {
                        if let title = selectedAnnotationTitle,
                            let name = array[i].name {
                            if title == name {
                                if let logo = array[i].logoUrl {
                                    if !logo.isEmpty {
                                        cell.imageCell.contentMode = .scaleAspectFit
                                        cell.imageCell.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(logo))!)
                                    } else {
                                        cell.imageCell.contentMode = .scaleAspectFill
                                        cell.imageCell.image = UIImage(named: "empty_image")
                                    }
                                } else {
                                    cell.imageCell.contentMode = .scaleAspectFill
                                    cell.imageCell.image = UIImage(named: "empty_image")
                                }
                                
                                if let title = selectedAnnotationTitle {
                                    if !title.isEmpty {
                                        cell.labelName.text = title
                                    }
                                }
                                
                                if let address = array[i].address {
                                    if let fullAddress = address.fullAddress {
                                        if !fullAddress.isEmpty {
                                            cell.labelAddress.text = fullAddress
                                        }
                                    }
                                }
                                
                                if self.distanceArray.count == 1 {
                                    cell.labelDistance.text = String(format:"%.2f", self.distanceArray[(indexPath as NSIndexPath).row]/1000) + " km"
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if let model = viewModel {
                if let array = model.dataArray {
                    if let logo = array[(indexPath as NSIndexPath).row].logoUrl {
                        if !logo.isEmpty {
                            cell.imageCell.contentMode = .scaleAspectFit
                            cell.imageCell.af_setImage(withURL: URL(string: NetworkConstant.APIPath.ProfileImage(logo))!)
                        } else {
                            cell.imageCell.contentMode = .scaleAspectFill
                            cell.imageCell.image = UIImage(named: "empty_image")
                        }
                    } else {
                        cell.imageCell.contentMode = .scaleAspectFill
                        cell.imageCell.image = UIImage(named: "empty_image")
                    }
                    
                    if let name = array[(indexPath as NSIndexPath).row].name {
                        if !name.isEmpty {
                            cell.labelName.text = name
                        }
                    }
                    
                    if let address = array[(indexPath as NSIndexPath).row].address {
                        if let fullAddress = address.fullAddress {
                            if !fullAddress.isEmpty {
                                cell.labelAddress.text = fullAddress
                            }
                        }
                    }
                    
                    if self.distanceArray.count == array.count {
                        cell.labelDistance.text = String(format:"%.2f", self.distanceArray[(indexPath as NSIndexPath).row]/1000) + " km"
                    }
                }
            }
        }
        
        if let model = viewModel {
            if let array = model.dataArray {
                if ((indexPath as NSIndexPath).row == array.count - 1) {
                    if let pagination = model.paginationResponse {
                        if ((pagination.last) != nil && pagination.last == false) {
                            model.reloadContent()
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = viewModel {
            model.animateViewMoving(false)
        }
        reloadSelectAnnotation()
        restaurantSearchBar.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == SegueConstant.Segue.restaurantDetailContainer {
            if let vc = segue.destination as? RestaurantDetailContainerViewController {
                if isSearching {
                    if let tv = restaurantTableView {
                        if let row = (tv.indexPathForSelectedRow as NSIndexPath?)?.row {
                            vc.passedObject = filtered[row]
                            trackEvent(row)
                        }
                    }
                } else if isSelectedAnnotation {
                    if let model = viewModel {
                        if let array = model.dataArray {
                            for i in 0 ..< array.count {
                                if let name = array[i].name {
                                    if selectedAnnotationTitle == name {
                                        vc.passedObject = array[i]
                                        trackEvent(i)
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if let model = viewModel {
                        if let array = model.dataArray,
                            let tv = restaurantTableView {
                            if let row = (tv.indexPathForSelectedRow as NSIndexPath?)?.row {
                                vc.passedObject = array[row]
                                trackEvent(row)
                            }
                        }
                    }
                }
            }
        }
    }
    
    ///////////////////////////////////SEARCH BAR///////////////////////////////////
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text!.isEmpty {
            isSearching = false
        } else {
            isSearching = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        resetSearch(false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.endEditing(true)
    }
    
    func resetSearch(_ isTapOut:Bool) {
        isSearching = false
        filtered.removeAll()
        if isTapOut {
            restaurantSearchBar.text = ""
            restaurantSearchBar.endEditing(true)
            reloadTableView()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let model = viewModel {
            if let array = model.dataArray {
                filtered = array.filter({ (text) -> Bool in
                    let tmp: NSString = text.name! as NSString
                    let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                    return range.location != NSNotFound
                })
            }
        }
        
        if filtered.count == 0 {
            isSearching = false
        } else {
            isSearching = true
        }
        reloadTableView()
        calculateDistance()
    }
    
    @IBAction func dissmissKeyboardOnTap(_ sender: AnyObject) {
        resetSearch(true)
    }
    
    ///////////////////////////////////SWITCH SEARCH///////////////////////////////////
    
    @IBAction func onSearchPressed(_ sender: AnyObject) {
        if let model = viewModel {
            spaceView.isHidden = true
            model.animateViewMoving(true)
            restaurantSearchBar.becomeFirstResponder()
        }
    }
    @IBAction func onBackToMapPressed(_ sender: AnyObject) {
        if let model = viewModel {
            spaceView.isHidden = false
            model.animateViewMoving(false)
            reloadWhenBackToMapPressed()
        }
    }
    
    func reloadWhenBackToMapPressed() {
        isSearching = false
        restaurantSearchBar.resignFirstResponder()
        restaurantSearchBar.text = ""
        calculateDistance()
    }
    
    func initActivityIndicator() {
        let activityTypes: [NVActivityIndicatorType] = [
            .ballPulse,
            .ballGridPulse,
            .ballClipRotate,
            .ballClipRotatePulse,
            .squareSpin,
            .ballClipRotateMultiple,
            .ballPulseRise,
            .ballRotate,
            .cubeTransition,
            .ballZigZag,
            .ballZigZagDeflect,
            .ballTrianglePath,
            .ballScale,
            .lineScale,
            .lineScaleParty,
            .ballScaleMultiple,
            .ballPulseSync,
            .ballBeat,
            .lineScalePulseOut,
            .lineScalePulseOutRapid,
            .ballScaleRipple,
            .ballScaleRippleMultiple,
            .ballSpinFadeLoader,
            .lineSpinFadeLoader,
            .triangleSkewSpin,
            .pacman,
            .ballGridBeat,
            .semiCircleSpin,
            .ballRotateChase]
        
        let frame = CGRect(x: self.view.frame.width/2 - (self.view.frame.width/12)/2, y: self.view.frame.height/3, width: self.view.frame.width/12, height: self.view.frame.width/12)
        activityIndicatorView = NVActivityIndicatorView(frame: frame, type: activityTypes[13])
        self.view.addSubview(activityIndicatorView!)
    }
}
