//
//  LoginViewController.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 10/7/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

import UIKit
import UITextField_Shake
import Alamofire

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textFieldForPassword: UITextField!
    @IBOutlet weak var textFieldForUsername: UITextField!
    @IBOutlet weak var labelErrorMessage: UILabel!
    let viewModel = LoginViewModel()
    let track = WMCLoyaltyAnalyticsTracking()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFirst()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        addScreenTracking()
    }
    
    func addScreenTracking() {
        track.sendScreenView(TrackingConstant.Screen.login_screen)
    }
    
    func initFirst() {
        viewModel.delegate = self
        Helper.setTextFieldBorder(textFieldForUsername)
        Helper.setTextFieldBorder(textFieldForPassword)
        textFieldForUsername.text = "880020001701@wmcprestige.vn"
        textFieldForPassword.text = "wmc.loyalty"
    }
    
    @IBAction func onBackPressed(_ sender: AnyObject) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func dissmissKeyboardOnTap(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @IBAction func onInfoPressed(_ sender: AnyObject) {
        Alert.hostOption(self, message: "Stable 192.168.1.88\nTest 192.168.1.88:8081")
    }
    
    @IBAction func onLoginPressed(_ sender: AnyObject) {
        if let navigation = self.navigationController {
            viewModel.processLogin(navigation)
            track.trackEvent(TrackingConstant.Screen.login_screen, action: TrackingConstant.Action.login, label: "", value: 0)
        }
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewModel.moveViewUp(true, delegate: self)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.moveViewUp(false, delegate: self)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return Helper.limitCharacter(textField, range: range, string: string)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let navigation = self.navigationController {
            viewModel.processLogin(navigation)
            track.trackEvent(TrackingConstant.Screen.login_screen, action: TrackingConstant.Action.login, label: "", value: 0)
        }
        return true
    }
}
