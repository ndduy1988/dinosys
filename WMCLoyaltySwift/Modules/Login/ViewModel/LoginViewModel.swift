//
//  LoginViewModel.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/27/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit
import PKHUD

class LoginViewModel {
    var isUserName: Bool?
    var isPassword: Bool?
    var delegate: LoginViewController?
    
    func processLogin(_ navigation:UINavigationController) {
        if let dele = delegate {
            if let userName = dele.textFieldForUsername,
                let passWord = dele.textFieldForPassword,
                let error = dele.labelErrorMessage {
                isUserName = checkTextField(userName, labelError: error, isCheckLoyaltyNumber:false, isCheckEmail:false)
                isPassword = checkTextField(passWord, labelError: error, isCheckLoyaltyNumber:false, isCheckEmail:false)
                
                if isUserName! && isPassword! {
                    if let name = userName.text,
                        let intValue = Int64(name) {
                        if Helper.isValidLoyaltyNumber(String(intValue)) {
                            onLogin(navigation, userName: userName, passWord: passWord)
                            Helper.dismissKeyboard(userName, passWord: passWord)
                        } else {
                            let check = checkTextField(userName, labelError: error, isCheckLoyaltyNumber:true, isCheckEmail:false)
                            print(check)
                        }
                    } else {
                        if let name = userName.text {
                            if Helper.isValidEmail(name) {
                                onLogin(navigation, userName: userName, passWord: passWord)
                                Helper.dismissKeyboard(userName, passWord: passWord)
                            } else {
                                let check = checkTextField(userName, labelError: error, isCheckLoyaltyNumber:false, isCheckEmail:true)
                                print(check)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func onLogin(_ navigation:UINavigationController, userName:UITextField, passWord:UITextField) {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        if let name = userName.text,
            let pass = passWord.text {
            WMCLoyaltyLoginRequest.init(callback: { (data:AuthenticationResponse?, error:ErrorResponse?) -> Void in
                if let _ = data {
                    PKHUD.sharedHUD.contentView = PKHUDSuccessView()
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay:0.5)
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationConstants.Navigation.kNotificationRequestNavigation), object: NotificationConstants.ViewController.ProfileViewController)
                    navigation.popViewController(animated: true)
                } else {
                    PKHUD.sharedHUD.contentView = PKHUDErrorView(subtitle: "Incorrect login credentials")
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay:1.5)
                }
                }, username: name, password: pass).execute()
        }
    }
    
    func checkTextField(_ textField: UITextField, labelError:UILabel, isCheckLoyaltyNumber:Bool, isCheckEmail:Bool) -> Bool {
        textField.layer.masksToBounds = true
        textField.layer.borderWidth = 1
        if isCheckLoyaltyNumber {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.shake(10, withDelta: 1.0, speed: 0.05)
            labelError.text = "The loyalty number contain 12 numbers."
            labelError.isHidden = false
            return false
        } else if isCheckEmail {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.shake(10, withDelta: 1.0, speed: 0.05)
            labelError.text = "That is not a valid email address."
            labelError.isHidden = false
            return false
        } else {
            if let tf = textField.text {
                if tf.isEmpty {
                    textField.layer.borderColor = UIColor.red.cgColor
                    textField.shake(10, withDelta: 1.0, speed: 0.05)
                    labelError.isHidden = true
                    return false
                }
            }
        }
        labelError.isHidden = true
        textField.layer.borderColor = UIColor.gray.cgColor
        return true
    }
    
    //view will move up on iPhone 3.5 inchs
    func moveViewUp(_ isUp: Bool, delegate: UIViewController) {
        if DeviceType.IS_IPHONE_4_OR_LESS {
            if isUp {
                animateViewMoving(true, moveValue: 40, delegate: delegate)
            } else {
                animateViewMoving(false, moveValue: 40, delegate: delegate)
            }
        }
    }
    
    //moving up
    func animateViewMoving (_ up:Bool, moveValue :CGFloat, delegate: UIViewController) {
        let movementDuration:TimeInterval = 0.5
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        delegate.view.frame = delegate.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}
