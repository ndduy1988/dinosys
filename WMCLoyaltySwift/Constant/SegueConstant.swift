//
//  SegueConstant.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/5/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

struct SegueConstant {
    struct Segue {
        static let promotionDetailContainer = "promotionDetailContainerSegue"
        static let restaurantDetailContainer = "restaurantDetailContainerSegue"
        static let hotelDetailContainer = "hotelDetailContainerSegue"
        static let activitiesDetailContainer = "activitiesDetailContainerSegue"
        static let userSetting = "userSettingSegue"
        static let aboutContainer = "aboutContainerSegue"
        static let userSettingContainer = "userSettingContainerSegue"
        static let hotelBookingContainer = "hotelBookingContainerSegue"
        static let restaurantBookingContainer = "restaurantBookingContainerSegue"
        static let rulesDetail = "rulesDetailSegue"
    }
    
    struct EmbedSegue {
        static let profilePage = "profilePageEmbedSegue"
        static let userSettingDetail = "userSettingDetailEmbedSegue"
        static let activitiesDetail = "activitiesDetailEmbedSegue"
        static let hotelDetail = "hotelDetailEmbedSegue"
        static let hotelBookingDetail = "hotelBookingDetailEmbedSegue"
        static let restaurantDetail = "restaurantDetailEmbedSegue"
        static let restaurantBookingDetail = "restaurantBookingDetailEmbedSegue"
        static let promotionDetail = "promotionDetailEmbedSegue"
        static let rules = "rulesEmbedSegue"
    }
}
