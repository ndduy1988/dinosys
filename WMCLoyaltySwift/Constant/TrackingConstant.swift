//
//  TrackingConstant.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 9/5/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

struct TrackingConstant {
    struct Screen {
        static let profile_screen = "PROFILE_SCREEN"
        static let campaign_list_screen = "CAMPAIGN_LIST_SCREEN"
        static let campaign_detail_screen = "CAMPAIGN_DETAIL_SCREEN"
        static let login_screen = "LOGIN_SCREEN"
        static let setting_screen = "SETTING_SCREEN"
        static let summary_screen = "SUMMARY_SCREEN"
        static let activities_list_screen = "ACTIVITIES_LIST_SCREEN"
        static let activities_detail_screen = "ACTIVITIES_DETAIL_SCREEN"
        static let about_screen = "ABOUT_SCREEN"
        static let restaurant_list_screen = "RESTAURANT_LIST_SCREEN"
        static let restaurant_detail_screen = "RESTAURANT_DETAIL_SCREEN"
        static let hotel_list_screen = "HOTEL_LIST_SCREEN"
        static let hotel_detail_screen = "HOTEL_DETAIL_SCREEN"
    }
    
    struct Action {
        static let swipe_campaign = "SWIPE_CAMPAIGN"
        static let login = "LOGIN"
        static let summary = "SUMMARY"
        static let activity = "ACTIVITY"
        static let swipe_campaign_image = "SWIPE_CAMPAIGN_IMAGE"
        static let change_image = "CHANGE_IMAGE"
        static let logout = "LOGOUT"
        static let campaign_select = "CAMPAIGN_SELECT"
        static let hotel_select = "HOTEL_SELECT"
        static let restaurant_select = "RESTAURANT_SELECT"
    }
}
