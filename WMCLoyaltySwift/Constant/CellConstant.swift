//
//  CellConstant.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/5/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

struct CellConstant {
    struct TableCellIdentifier {
        static let promotion = "promotionTableCell"
        static let restaurant = "restaurantTableCell"
        static let hotel = "hotelTableCell"
        static let activities = "activitiesTableCell"
        static let rules = "rulesTableCell"
        static let rulesDetail = "rulesDetailTableCell"
    }
    
    struct CollectionCellIdentifier {
        static let promotion = "promotionCollectionCell"
        static let promotionDetail = "promotionDetailCollectionCell"
    }
}
