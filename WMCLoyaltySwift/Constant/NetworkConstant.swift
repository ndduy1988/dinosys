//
//  NetworkConstant.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 7/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

struct NetworkConstant {
    
//    static var BaseURL = "http://192.168.1.88"
//    static var BaseURL = "http://14.161.21.179:8081"
    
    static var BaseURL = "https://api.dinosys.vn"
    
    static var isPinning = true
    
    struct HeaderKey {
        static let Authentication = "x-auth-token"
    }
    
    struct APIPath {
        static let Authenticate = "/api/authenticate"
        static let ForgotPassword = "/api/account/reset_password/init"
        static let Outlet = "/api/outlets"
        static let Account = "/api/profile"
        static let Transaction = "/api/profile/transactions"
        static let Member = "/api/members"
        static let Promotion = "/api/campaigns"
        static let ImageUpload = "/api/profile/avatar"
        
        //PROFILE IMAGE
        static func ProfileImage(_ path:String) -> String {
            return NetworkConstant.getFullPath(path)
        }
        //ACCOUNT
        static func TransactionOfAccountById(_ id:Int) -> String {
            return "/api/profile/transaction/\(id)"
        }
        //TRANSACTION
        static func TransactionById(_ id:Int) -> String {
            return "/api/transactions/id/\(id)"
        }
        //PROMOTION
        static func PromotionById(_ id:Int) -> String {
            return "/api/promotions/\(id)"
        }
        static func PromotionByOutletAndMember(_ outletCode:String, memberId:Int) -> String {
            return "/api/promotions/outlet/\(outletCode)/member/\(memberId)"
        }
        //OUTLET
        static func OutletByIdentifier(_ identifier:String) -> String {
            return "/api/outlets/identifier/\(identifier)"
        }
        static func OutletByType(_ type:EnumConstant.Outlet.OutletType) -> String {
            return "/api/outlets/type/\(type)"
        }
        static func OutletById(_ id:Int) -> String {
            return "/api/outlets/id/\(id)"
        }
    }
    
    static func getFullPath(_ path:String) -> String {
        return NetworkConstant.BaseURL + path
    }
}
