//
//  EnumConstant.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/4/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

struct EnumConstant {
    struct Outlet {
        enum OutletType {
            case restaurant
            case hotel
            case office
            case shopping
            case apartment
        }
    }
}
