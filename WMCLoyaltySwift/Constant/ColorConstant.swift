//
//  ColorConstant.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/4/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

struct ColorConstant {
    struct Outlet {
        //restaurant & hotel
        static let userLocation = UIColor(red: 255/255, green: 102/255, blue: 0/255, alpha: 1)
        static let routeLine = UIColor(red: 89/255, green: 89/255, blue: 104/255, alpha: 1)
    }
    
    struct Summary {
        //PNChart
        static let orangeColor = UIColor(red: 248/255, green: 234/255, blue: 165/255, alpha: 1.0)
        static let purpleColor = UIColor(red: 64/255, green: 42/255, blue: 84/255, alpha: 1)
        static let grayColor = UIColor(red: 230/255, green: 229/255, blue: 234/255, alpha: 1)
    }
    
    struct PromotionDetail {
        //popover
        static let purpleColor = UIColor(red: 101/255, green: 45/255, blue: 144/255, alpha: 1)
    }
}
