//
//  NotificationConstant.swift
//  WMCLoyaltySwift
//
//  Created by Duy Nguyen on 6/10/15.
//  Copyright © 2015 dinosys. All rights reserved.
//

struct NotificationConstants {
    struct Navigation {
        static let kNotificationRequestNavigation = "kNotificationRequestNavigation"
    }
    
    struct ViewController {
        //Tab Bar
        static let TabBarViewController = "TabBarViewController"
        
        //4 Tabs
        static let ProfileViewController = "ProfileViewController"
        static let PromotionViewController = "PromotionViewController"
        static let RestaurantViewController = "RestaurantViewController"
        static let HotelViewController = "HotelViewController"
        
        //General
        static let MainMenuViewController = "MainMenuViewController"
        static let LoginViewController = "LoginViewController"
        
        //Profile
        static let SummaryViewController = "SummaryViewController"
        static let ActivitiesViewController = "ActivitiesViewController"
        
        //Promotion
        static let PromotionDetailContainerViewController = "PromotionDetailContainerViewController"
        static let PromotionDetailViewController = "PromotionDetailViewController"
        
        //Main Menu Navigation
        static let MainMenuNavigationController = "MainMenuNavigationController"
        
        //Restaurant
        static let RestaurantDetailContainerViewController = "RestaurantDetailContainerViewController"
        
        //Hotel
        static let HotelDetailContainerViewController = "HotelDetailContainerViewController"
    }
    
    struct PushNotification {
        static let kAPNS = "kAPNS"
    }
}