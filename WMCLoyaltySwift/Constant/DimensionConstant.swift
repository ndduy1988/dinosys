//
//  DimensionConstant.swift
//  WMCLoyaltySwift
//
//  Created by Luân Trịnh on 1/7/16.
//  Copyright © 2016 dinosys. All rights reserved.
//

import UIKit

struct DimensionConstant {
    
    static let TABBAR_HEIGHT:CGFloat = 60
    
    struct Outlet {
        static let MAP_HEIGHT:CGFloat = 225
        static let RESTAURANT_CELL_HEIGHT:CGFloat = 90
        static let HOTEL_CELL_HEIGHT:CGFloat = 90
    }
    
    struct Image {
        static let IMAGE_SIZE:CGFloat = 256
        static let COMPRESSION_QUALITY:CGFloat = 0.75
    }
    
    struct PromotionLayout {
        static let ACTIVE_ZOOM:CGFloat = 200
        static let ZOOM_FACTOR:CGFloat = 0.15
        static let MAX_FLOAT:CGFloat = 0
    }
}
